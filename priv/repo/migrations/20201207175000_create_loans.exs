defmodule Portal.Repo.Migrations.CreateLoans do
  use Ecto.Migration

  def change do
    create table(:loans) do
      add :employee_id, :integer
      add :loanID, :integer
      add :principal, :decimal
      add :monthlyPaymentDate, :date
      add :amount, :decimal
      add :interest, :decimal
      add :amountDue, :decimal
      add :amountPaid, :decimal

      timestamps()
    end

  end
end

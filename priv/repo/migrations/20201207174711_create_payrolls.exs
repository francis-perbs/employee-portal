defmodule Portal.Repo.Migrations.CreatePayrolls do
  use Ecto.Migration

  def change do
    create table(:payrolls) do
      add :empCode, :string
      add :payrollCode, :string
      add :beginningDate, :date
      add :cutoffDate, :date
      add :status, :string
      add :bonus, :decimal
      add :thirteenthMonth, :decimal
      add :allowances, :decimal
      add :commission, :decimal
      add :tardiness, :decimal
      add :absences, :decimal
      add :loan, :decimal
      add :cashAdvances, :decimal

      timestamps()
    end

  end
end

defmodule Portal.Repo.Migrations.CreateEmployees do
  use Ecto.Migration

  def change do
    create table(:employees) do
      add :employeeCode, :string
      add :firstName, :string
      add :middleName, :string
      add :lastName, :string
      add :jobTitleID, :integer
      add :departmentID, :integer
      add :gender, :string
      add :status, :string
      add :birthDate, :date
      add :presentAddr, :string
      add :permanentAddr, :string
      add :countryID, :integer
      add :contactNbr, :string
      add :email, :string
      add :employeeStatus, :string
      add :hireDate, :date
      add :terminationDate, :date
      add :sssNbr, :string
      add :tinNbr, :string
      add :philhealthNbr, :string
      add :hdmfNbr, :string
      add :hmoNbr, :string
      add :bankAcctNbr, :string
      add :basicSalary, :decimal
      add :taxStatus, :string
      add :sssPrem, :decimal
      add :philhealthPrem, :decimal
      add :hdmfPrem, :decimal
      add :taxWth, :decimal
      add :allowance, :decimal

      timestamps()
    end

    create unique_index(:employees, [:employeeCode])
  end
end

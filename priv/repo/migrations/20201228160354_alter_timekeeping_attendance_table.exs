defmodule Portal.Repo.Migrations.AlterTimekeepingAttendanceTable do
  use Ecto.Migration

  def change do
    drop table(:attendances)

    create table(:attendances) do
      add :employee_id, :integer
      add :dateIn, :date
      add :timeIn, :time
      add :dateOut, :date
      add :timeOut, :time
      add :regHours, :decimal
      add :otHours, :decimal
      add :leaveHours, :decimal
      add :totalHours, :decimal

      timestamps()
    end
  end
end

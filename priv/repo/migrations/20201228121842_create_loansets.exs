defmodule Portal.Repo.Migrations.CreateLoansets do
  use Ecto.Migration

  def change do
    create table(:loansets) do
      add :name, :string

      timestamps()
    end

  end
end

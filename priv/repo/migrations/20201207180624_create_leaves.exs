defmodule Portal.Repo.Migrations.CreateLeaves do
  use Ecto.Migration

  def change do
    create table(:leaves) do
      add :name, :string

      timestamps()
    end

  end
end

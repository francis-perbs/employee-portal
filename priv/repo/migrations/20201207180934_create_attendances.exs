defmodule Portal.Repo.Migrations.CreateAttendances do
  use Ecto.Migration

  def change do
    create table(:attendances) do
      add :empCode, :string
      add :dateTimeIn, :utc_datetime
      add :dateTimeOut, :utc_datetime
      add :time_zone, :string
      add :original_offset, :integer
      add :workType, :string
      add :workHours, :decimal

      timestamps()
    end

  end
end

defmodule Portal.Repo.Migrations.CreateEmpLeaves do
  use Ecto.Migration

  def change do
    create table(:emp_leaves) do
      add :leaveType, :string
      add :leaveQty, :decimal
      add :employee_id, references(:employees, on_delete: :nothing)

      timestamps()
    end

    create index(:emp_leaves, [:employee_id])
  end
end

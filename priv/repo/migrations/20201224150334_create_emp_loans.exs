defmodule Portal.Repo.Migrations.CreateEmpLoans do
  use Ecto.Migration

  def change do
    create table(:emp_loans) do
      add :loanID, :integer
      add :loanName, :string
      add :loanDate, :date
      add :principalAmount, :decimal
      add :payableMonths, :decimal
      add :employee_id, references(:employees, on_delete: :nothing)

      timestamps()
    end

    create index(:emp_loans, [:employee_id])
  end
end

defmodule Portal.Repo.Migrations.CreateClaimDetails do
  use Ecto.Migration

  def change do
    create table(:claim_details) do
      add :expenseClaimNbr, :string
      add :expenseType, :string
      add :description, :string
      add :lineAmt, :decimal

      timestamps()
    end

  end
end

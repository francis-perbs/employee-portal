defmodule Portal.Repo.Migrations.CreateCountries do
  use Ecto.Migration

  def change do
    create table(:countries) do
      add :countryCD, :string
      add :countryName, :string

      timestamps()
    end

    create unique_index(:countries, [:countryCD], name: :countries_country_code_unique_index)
  end
end

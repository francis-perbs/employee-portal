defmodule Portal.Repo.Migrations.CreateClaims do
  use Ecto.Migration

  def change do
    create table(:claims) do
      add :expenseClaimNbr, :string
      add :description, :string
      add :status, :string
      add :expenseClaimDate, :date
      add :totalAmt, :decimal
      add :docDate, :date
      add :empCode, :string

      timestamps()
    end

  end
end

defmodule Portal.Repo.Migrations.CreatePositions do
  use Ecto.Migration

  def change do
    create table(:positions) do
      add :position, :string

      timestamps()
    end

  end
end

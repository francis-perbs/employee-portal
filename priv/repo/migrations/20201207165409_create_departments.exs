defmodule Portal.Repo.Migrations.CreateDepartments do
  use Ecto.Migration

  def change do
    create table(:departments) do
      add :departmentName, :string

      timestamps()
    end

  end
end

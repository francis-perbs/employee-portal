defmodule Portal.TimekeepingTest do
  use Portal.DataCase

  alias Portal.Timekeeping

  describe "attendances" do
    alias Portal.Timekeeping.Attendance

    @valid_attrs %{dateTimeIn: "2010-04-17T14:00:00Z", dateTimeOut: "2010-04-17T14:00:00Z", empCode: "some empCode", workHours: "120.5", workType: "some workType"}
    @update_attrs %{dateTimeIn: "2011-05-18T15:01:01Z", dateTimeOut: "2011-05-18T15:01:01Z", empCode: "some updated empCode", workHours: "456.7", workType: "some updated workType"}
    @invalid_attrs %{dateTimeIn: nil, dateTimeOut: nil, empCode: nil, workHours: nil, workType: nil}

    def attendance_fixture(attrs \\ %{}) do
      {:ok, attendance} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Timekeeping.create_attendance()

      attendance
    end

    test "list_attendances/0 returns all attendances" do
      attendance = attendance_fixture()
      assert Timekeeping.list_attendances() == [attendance]
    end

    test "get_attendance!/1 returns the attendance with given id" do
      attendance = attendance_fixture()
      assert Timekeeping.get_attendance!(attendance.id) == attendance
    end

    test "create_attendance/1 with valid data creates a attendance" do
      assert {:ok, %Attendance{} = attendance} = Timekeeping.create_attendance(@valid_attrs)
      assert attendance.dateTimeIn == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert attendance.dateTimeOut == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert attendance.empCode == "some empCode"
      assert attendance.workHours == Decimal.new("120.5")
      assert attendance.workType == "some workType"
    end

    test "create_attendance/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Timekeeping.create_attendance(@invalid_attrs)
    end

    test "update_attendance/2 with valid data updates the attendance" do
      attendance = attendance_fixture()
      assert {:ok, %Attendance{} = attendance} = Timekeeping.update_attendance(attendance, @update_attrs)
      assert attendance.dateTimeIn == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert attendance.dateTimeOut == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert attendance.empCode == "some updated empCode"
      assert attendance.workHours == Decimal.new("456.7")
      assert attendance.workType == "some updated workType"
    end

    test "update_attendance/2 with invalid data returns error changeset" do
      attendance = attendance_fixture()
      assert {:error, %Ecto.Changeset{}} = Timekeeping.update_attendance(attendance, @invalid_attrs)
      assert attendance == Timekeeping.get_attendance!(attendance.id)
    end

    test "delete_attendance/1 deletes the attendance" do
      attendance = attendance_fixture()
      assert {:ok, %Attendance{}} = Timekeeping.delete_attendance(attendance)
      assert_raise Ecto.NoResultsError, fn -> Timekeeping.get_attendance!(attendance.id) end
    end

    test "change_attendance/1 returns a attendance changeset" do
      attendance = attendance_fixture()
      assert %Ecto.Changeset{} = Timekeeping.change_attendance(attendance)
    end
  end
end

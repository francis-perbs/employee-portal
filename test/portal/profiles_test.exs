defmodule Portal.ProfilesTest do
  use Portal.DataCase

  alias Portal.Profiles

  describe "employees" do
    alias Portal.Profiles.Employee

    @valid_attrs %{allowance: "120.5", bankAcctNbr: "some bankAcctNbr", basicSalary: "120.5", birthDate: ~D[2010-04-17], contactNbr: "some contactNbr", countryID: 42, departmentID: 42, email: "some email", employeeCode: "some employeeCode", employeeStatus: "some employeeStatus", firstName: "some firstName", gender: "some gender", hdmfNbr: "some hdmfNbr", hdmfPrem: "120.5", hireDate: ~D[2010-04-17], hmoNbr: "some hmoNbr", jobTitleID: 42, lastName: "some lastName", middleName: "some middleName", permanentAddr: "some permanentAddr", philhealthNbr: "some philhealthNbr", philhealthPrem: "120.5", presentAddr: "some presentAddr", sssNbr: "some sssNbr", sssPrem: "120.5", status: "some status", taxStatus: "some taxStatus", taxWth: "120.5", terminationDate: ~D[2010-04-17], tinNbr: "some tinNbr"}
    @update_attrs %{allowance: "456.7", bankAcctNbr: "some updated bankAcctNbr", basicSalary: "456.7", birthDate: ~D[2011-05-18], contactNbr: "some updated contactNbr", countryID: 43, departmentID: 43, email: "some updated email", employeeCode: "some updated employeeCode", employeeStatus: "some updated employeeStatus", firstName: "some updated firstName", gender: "some updated gender", hdmfNbr: "some updated hdmfNbr", hdmfPrem: "456.7", hireDate: ~D[2011-05-18], hmoNbr: "some updated hmoNbr", jobTitleID: 43, lastName: "some updated lastName", middleName: "some updated middleName", permanentAddr: "some updated permanentAddr", philhealthNbr: "some updated philhealthNbr", philhealthPrem: "456.7", presentAddr: "some updated presentAddr", sssNbr: "some updated sssNbr", sssPrem: "456.7", status: "some updated status", taxStatus: "some updated taxStatus", taxWth: "456.7", terminationDate: ~D[2011-05-18], tinNbr: "some updated tinNbr"}
    @invalid_attrs %{allowance: nil, bankAcctNbr: nil, basicSalary: nil, birthDate: nil, contactNbr: nil, countryID: nil, departmentID: nil, email: nil, employeeCode: nil, employeeStatus: nil, firstName: nil, gender: nil, hdmfNbr: nil, hdmfPrem: nil, hireDate: nil, hmoNbr: nil, jobTitleID: nil, lastName: nil, middleName: nil, permanentAddr: nil, philhealthNbr: nil, philhealthPrem: nil, presentAddr: nil, sssNbr: nil, sssPrem: nil, status: nil, taxStatus: nil, taxWth: nil, terminationDate: nil, tinNbr: nil}

    def employee_fixture(attrs \\ %{}) do
      {:ok, employee} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Profiles.create_employee()

      employee
    end

    test "list_employees/0 returns all employees" do
      employee = employee_fixture()
      assert Profiles.list_employees() == [employee]
    end

    test "get_employee!/1 returns the employee with given id" do
      employee = employee_fixture()
      assert Profiles.get_employee!(employee.id) == employee
    end

    test "create_employee/1 with valid data creates a employee" do
      assert {:ok, %Employee{} = employee} = Profiles.create_employee(@valid_attrs)
      assert employee.allowance == Decimal.new("120.5")
      assert employee.bankAcctNbr == "some bankAcctNbr"
      assert employee.basicSalary == Decimal.new("120.5")
      assert employee.birthDate == ~D[2010-04-17]
      assert employee.contactNbr == "some contactNbr"
      assert employee.countryID == 42
      assert employee.departmentID == 42
      assert employee.email == "some email"
      assert employee.employeeCode == "some employeeCode"
      assert employee.employeeStatus == "some employeeStatus"
      assert employee.firstName == "some firstName"
      assert employee.gender == "some gender"
      assert employee.hdmfNbr == "some hdmfNbr"
      assert employee.hdmfPrem == Decimal.new("120.5")
      assert employee.hireDate == ~D[2010-04-17]
      assert employee.hmoNbr == "some hmoNbr"
      assert employee.jobTitleID == 42
      assert employee.lastName == "some lastName"
      assert employee.middleName == "some middleName"
      assert employee.permanentAddr == "some permanentAddr"
      assert employee.philhealthNbr == "some philhealthNbr"
      assert employee.philhealthPrem == Decimal.new("120.5")
      assert employee.presentAddr == "some presentAddr"
      assert employee.sssNbr == "some sssNbr"
      assert employee.sssPrem == Decimal.new("120.5")
      assert employee.status == "some status"
      assert employee.taxStatus == "some taxStatus"
      assert employee.taxWth == Decimal.new("120.5")
      assert employee.terminationDate == ~D[2010-04-17]
      assert employee.tinNbr == "some tinNbr"
    end

    test "create_employee/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Profiles.create_employee(@invalid_attrs)
    end

    test "update_employee/2 with valid data updates the employee" do
      employee = employee_fixture()
      assert {:ok, %Employee{} = employee} = Profiles.update_employee(employee, @update_attrs)
      assert employee.allowance == Decimal.new("456.7")
      assert employee.bankAcctNbr == "some updated bankAcctNbr"
      assert employee.basicSalary == Decimal.new("456.7")
      assert employee.birthDate == ~D[2011-05-18]
      assert employee.contactNbr == "some updated contactNbr"
      assert employee.countryID == 43
      assert employee.departmentID == 43
      assert employee.email == "some updated email"
      assert employee.employeeCode == "some updated employeeCode"
      assert employee.employeeStatus == "some updated employeeStatus"
      assert employee.firstName == "some updated firstName"
      assert employee.gender == "some updated gender"
      assert employee.hdmfNbr == "some updated hdmfNbr"
      assert employee.hdmfPrem == Decimal.new("456.7")
      assert employee.hireDate == ~D[2011-05-18]
      assert employee.hmoNbr == "some updated hmoNbr"
      assert employee.jobTitleID == 43
      assert employee.lastName == "some updated lastName"
      assert employee.middleName == "some updated middleName"
      assert employee.permanentAddr == "some updated permanentAddr"
      assert employee.philhealthNbr == "some updated philhealthNbr"
      assert employee.philhealthPrem == Decimal.new("456.7")
      assert employee.presentAddr == "some updated presentAddr"
      assert employee.sssNbr == "some updated sssNbr"
      assert employee.sssPrem == Decimal.new("456.7")
      assert employee.status == "some updated status"
      assert employee.taxStatus == "some updated taxStatus"
      assert employee.taxWth == Decimal.new("456.7")
      assert employee.terminationDate == ~D[2011-05-18]
      assert employee.tinNbr == "some updated tinNbr"
    end

    test "update_employee/2 with invalid data returns error changeset" do
      employee = employee_fixture()
      assert {:error, %Ecto.Changeset{}} = Profiles.update_employee(employee, @invalid_attrs)
      assert employee == Profiles.get_employee!(employee.id)
    end

    test "delete_employee/1 deletes the employee" do
      employee = employee_fixture()
      assert {:ok, %Employee{}} = Profiles.delete_employee(employee)
      assert_raise Ecto.NoResultsError, fn -> Profiles.get_employee!(employee.id) end
    end

    test "change_employee/1 returns a employee changeset" do
      employee = employee_fixture()
      assert %Ecto.Changeset{} = Profiles.change_employee(employee)
    end
  end

  describe "emp_leaves" do
    alias Portal.Profiles.EmpLeave

    @valid_attrs %{leaveQty: "120.5", leaveType: "some leaveType"}
    @update_attrs %{leaveQty: "456.7", leaveType: "some updated leaveType"}
    @invalid_attrs %{leaveQty: nil, leaveType: nil}

    def emp_leave_fixture(attrs \\ %{}) do
      {:ok, emp_leave} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Profiles.create_emp_leave()

      emp_leave
    end

    test "list_emp_leaves/0 returns all emp_leaves" do
      emp_leave = emp_leave_fixture()
      assert Profiles.list_emp_leaves() == [emp_leave]
    end

    test "get_emp_leave!/1 returns the emp_leave with given id" do
      emp_leave = emp_leave_fixture()
      assert Profiles.get_emp_leave!(emp_leave.id) == emp_leave
    end

    test "create_emp_leave/1 with valid data creates a emp_leave" do
      assert {:ok, %EmpLeave{} = emp_leave} = Profiles.create_emp_leave(@valid_attrs)
      assert emp_leave.leaveQty == Decimal.new("120.5")
      assert emp_leave.leaveType == "some leaveType"
    end

    test "create_emp_leave/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Profiles.create_emp_leave(@invalid_attrs)
    end

    test "update_emp_leave/2 with valid data updates the emp_leave" do
      emp_leave = emp_leave_fixture()
      assert {:ok, %EmpLeave{} = emp_leave} = Profiles.update_emp_leave(emp_leave, @update_attrs)
      assert emp_leave.leaveQty == Decimal.new("456.7")
      assert emp_leave.leaveType == "some updated leaveType"
    end

    test "update_emp_leave/2 with invalid data returns error changeset" do
      emp_leave = emp_leave_fixture()
      assert {:error, %Ecto.Changeset{}} = Profiles.update_emp_leave(emp_leave, @invalid_attrs)
      assert emp_leave == Profiles.get_emp_leave!(emp_leave.id)
    end

    test "delete_emp_leave/1 deletes the emp_leave" do
      emp_leave = emp_leave_fixture()
      assert {:ok, %EmpLeave{}} = Profiles.delete_emp_leave(emp_leave)
      assert_raise Ecto.NoResultsError, fn -> Profiles.get_emp_leave!(emp_leave.id) end
    end

    test "change_emp_leave/1 returns a emp_leave changeset" do
      emp_leave = emp_leave_fixture()
      assert %Ecto.Changeset{} = Profiles.change_emp_leave(emp_leave)
    end
  end

  describe "emp_loans" do
    alias Portal.Profiles.EmpLoan

    @valid_attrs %{loanDate: ~D[2010-04-17], loanType: "some loanType", payableMonths: "120.5", principalAmt: "120.5"}
    @update_attrs %{loanDate: ~D[2011-05-18], loanType: "some updated loanType", payableMonths: "456.7", principalAmt: "456.7"}
    @invalid_attrs %{loanDate: nil, loanType: nil, payableMonths: nil, principalAmt: nil}

    def emp_loan_fixture(attrs \\ %{}) do
      {:ok, emp_loan} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Profiles.create_emp_loan()

      emp_loan
    end

    test "list_emp_loans/0 returns all emp_loans" do
      emp_loan = emp_loan_fixture()
      assert Profiles.list_emp_loans() == [emp_loan]
    end

    test "get_emp_loan!/1 returns the emp_loan with given id" do
      emp_loan = emp_loan_fixture()
      assert Profiles.get_emp_loan!(emp_loan.id) == emp_loan
    end

    test "create_emp_loan/1 with valid data creates a emp_loan" do
      assert {:ok, %EmpLoan{} = emp_loan} = Profiles.create_emp_loan(@valid_attrs)
      assert emp_loan.loanDate == ~D[2010-04-17]
      assert emp_loan.loanType == "some loanType"
      assert emp_loan.payableMonths == Decimal.new("120.5")
      assert emp_loan.principalAmt == Decimal.new("120.5")
    end

    test "create_emp_loan/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Profiles.create_emp_loan(@invalid_attrs)
    end

    test "update_emp_loan/2 with valid data updates the emp_loan" do
      emp_loan = emp_loan_fixture()
      assert {:ok, %EmpLoan{} = emp_loan} = Profiles.update_emp_loan(emp_loan, @update_attrs)
      assert emp_loan.loanDate == ~D[2011-05-18]
      assert emp_loan.loanType == "some updated loanType"
      assert emp_loan.payableMonths == Decimal.new("456.7")
      assert emp_loan.principalAmt == Decimal.new("456.7")
    end

    test "update_emp_loan/2 with invalid data returns error changeset" do
      emp_loan = emp_loan_fixture()
      assert {:error, %Ecto.Changeset{}} = Profiles.update_emp_loan(emp_loan, @invalid_attrs)
      assert emp_loan == Profiles.get_emp_loan!(emp_loan.id)
    end

    test "delete_emp_loan/1 deletes the emp_loan" do
      emp_loan = emp_loan_fixture()
      assert {:ok, %EmpLoan{}} = Profiles.delete_emp_loan(emp_loan)
      assert_raise Ecto.NoResultsError, fn -> Profiles.get_emp_loan!(emp_loan.id) end
    end

    test "change_emp_loan/1 returns a emp_loan changeset" do
      emp_loan = emp_loan_fixture()
      assert %Ecto.Changeset{} = Profiles.change_emp_loan(emp_loan)
    end
  end

  describe "emp_leaves" do
    alias Portal.Profiles.Empleave

    @valid_attrs %{leaveQty: "120.5", leaveType: "some leaveType"}
    @update_attrs %{leaveQty: "456.7", leaveType: "some updated leaveType"}
    @invalid_attrs %{leaveQty: nil, leaveType: nil}

    def empleave_fixture(attrs \\ %{}) do
      {:ok, empleave} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Profiles.create_empleave()

      empleave
    end

    test "list_emp_leaves/0 returns all emp_leaves" do
      empleave = empleave_fixture()
      assert Profiles.list_emp_leaves() == [empleave]
    end

    test "get_empleave!/1 returns the empleave with given id" do
      empleave = empleave_fixture()
      assert Profiles.get_empleave!(empleave.id) == empleave
    end

    test "create_empleave/1 with valid data creates a empleave" do
      assert {:ok, %Empleave{} = empleave} = Profiles.create_empleave(@valid_attrs)
      assert empleave.leaveQty == Decimal.new("120.5")
      assert empleave.leaveType == "some leaveType"
    end

    test "create_empleave/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Profiles.create_empleave(@invalid_attrs)
    end

    test "update_empleave/2 with valid data updates the empleave" do
      empleave = empleave_fixture()
      assert {:ok, %Empleave{} = empleave} = Profiles.update_empleave(empleave, @update_attrs)
      assert empleave.leaveQty == Decimal.new("456.7")
      assert empleave.leaveType == "some updated leaveType"
    end

    test "update_empleave/2 with invalid data returns error changeset" do
      empleave = empleave_fixture()
      assert {:error, %Ecto.Changeset{}} = Profiles.update_empleave(empleave, @invalid_attrs)
      assert empleave == Profiles.get_empleave!(empleave.id)
    end

    test "delete_empleave/1 deletes the empleave" do
      empleave = empleave_fixture()
      assert {:ok, %Empleave{}} = Profiles.delete_empleave(empleave)
      assert_raise Ecto.NoResultsError, fn -> Profiles.get_empleave!(empleave.id) end
    end

    test "change_empleave/1 returns a empleave changeset" do
      empleave = empleave_fixture()
      assert %Ecto.Changeset{} = Profiles.change_empleave(empleave)
    end
  end

  describe "emp_loans" do
    alias Portal.Profiles.Emploan

    @valid_attrs %{loanDate: ~D[2010-04-17], loanID: 42, loanName: "some loanName", payableMonths: "120.5", principalAmount: "120.5"}
    @update_attrs %{loanDate: ~D[2011-05-18], loanID: 43, loanName: "some updated loanName", payableMonths: "456.7", principalAmount: "456.7"}
    @invalid_attrs %{loanDate: nil, loanID: nil, loanName: nil, payableMonths: nil, principalAmount: nil}

    def emploan_fixture(attrs \\ %{}) do
      {:ok, emploan} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Profiles.create_emploan()

      emploan
    end

    test "list_emp_loans/0 returns all emp_loans" do
      emploan = emploan_fixture()
      assert Profiles.list_emp_loans() == [emploan]
    end

    test "get_emploan!/1 returns the emploan with given id" do
      emploan = emploan_fixture()
      assert Profiles.get_emploan!(emploan.id) == emploan
    end

    test "create_emploan/1 with valid data creates a emploan" do
      assert {:ok, %Emploan{} = emploan} = Profiles.create_emploan(@valid_attrs)
      assert emploan.loanDate == ~D[2010-04-17]
      assert emploan.loanID == 42
      assert emploan.loanName == "some loanName"
      assert emploan.payableMonths == Decimal.new("120.5")
      assert emploan.principalAmount == Decimal.new("120.5")
    end

    test "create_emploan/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Profiles.create_emploan(@invalid_attrs)
    end

    test "update_emploan/2 with valid data updates the emploan" do
      emploan = emploan_fixture()
      assert {:ok, %Emploan{} = emploan} = Profiles.update_emploan(emploan, @update_attrs)
      assert emploan.loanDate == ~D[2011-05-18]
      assert emploan.loanID == 43
      assert emploan.loanName == "some updated loanName"
      assert emploan.payableMonths == Decimal.new("456.7")
      assert emploan.principalAmount == Decimal.new("456.7")
    end

    test "update_emploan/2 with invalid data returns error changeset" do
      emploan = emploan_fixture()
      assert {:error, %Ecto.Changeset{}} = Profiles.update_emploan(emploan, @invalid_attrs)
      assert emploan == Profiles.get_emploan!(emploan.id)
    end

    test "delete_emploan/1 deletes the emploan" do
      emploan = emploan_fixture()
      assert {:ok, %Emploan{}} = Profiles.delete_emploan(emploan)
      assert_raise Ecto.NoResultsError, fn -> Profiles.get_emploan!(emploan.id) end
    end

    test "change_emploan/1 returns a emploan changeset" do
      emploan = emploan_fixture()
      assert %Ecto.Changeset{} = Profiles.change_emploan(emploan)
    end
  end
end

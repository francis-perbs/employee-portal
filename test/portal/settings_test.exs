defmodule Portal.SettingsTest do
  use Portal.DataCase

  alias Portal.Settings

  describe "countries" do
    alias Portal.Settings.Country

    @valid_attrs %{countryCD: "some countryCD", countryName: "some countryName"}
    @update_attrs %{countryCD: "some updated countryCD", countryName: "some updated countryName"}
    @invalid_attrs %{countryCD: nil, countryName: nil}

    def country_fixture(attrs \\ %{}) do
      {:ok, country} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Settings.create_country()

      country
    end

    test "list_countries/0 returns all countries" do
      country = country_fixture()
      assert Settings.list_countries() == [country]
    end

    test "get_country!/1 returns the country with given id" do
      country = country_fixture()
      assert Settings.get_country!(country.id) == country
    end

    test "create_country/1 with valid data creates a country" do
      assert {:ok, %Country{} = country} = Settings.create_country(@valid_attrs)
      assert country.countryCD == "some countryCD"
      assert country.countryName == "some countryName"
    end

    test "create_country/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Settings.create_country(@invalid_attrs)
    end

    test "update_country/2 with valid data updates the country" do
      country = country_fixture()
      assert {:ok, %Country{} = country} = Settings.update_country(country, @update_attrs)
      assert country.countryCD == "some updated countryCD"
      assert country.countryName == "some updated countryName"
    end

    test "update_country/2 with invalid data returns error changeset" do
      country = country_fixture()
      assert {:error, %Ecto.Changeset{}} = Settings.update_country(country, @invalid_attrs)
      assert country == Settings.get_country!(country.id)
    end

    test "delete_country/1 deletes the country" do
      country = country_fixture()
      assert {:ok, %Country{}} = Settings.delete_country(country)
      assert_raise Ecto.NoResultsError, fn -> Settings.get_country!(country.id) end
    end

    test "change_country/1 returns a country changeset" do
      country = country_fixture()
      assert %Ecto.Changeset{} = Settings.change_country(country)
    end
  end

  describe "departments" do
    alias Portal.Settings.Department

    @valid_attrs %{departmentName: "some departmentName"}
    @update_attrs %{departmentName: "some updated departmentName"}
    @invalid_attrs %{departmentName: nil}

    def department_fixture(attrs \\ %{}) do
      {:ok, department} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Settings.create_department()

      department
    end

    test "list_departments/0 returns all departments" do
      department = department_fixture()
      assert Settings.list_departments() == [department]
    end

    test "get_department!/1 returns the department with given id" do
      department = department_fixture()
      assert Settings.get_department!(department.id) == department
    end

    test "create_department/1 with valid data creates a department" do
      assert {:ok, %Department{} = department} = Settings.create_department(@valid_attrs)
      assert department.departmentName == "some departmentName"
    end

    test "create_department/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Settings.create_department(@invalid_attrs)
    end

    test "update_department/2 with valid data updates the department" do
      department = department_fixture()
      assert {:ok, %Department{} = department} = Settings.update_department(department, @update_attrs)
      assert department.departmentName == "some updated departmentName"
    end

    test "update_department/2 with invalid data returns error changeset" do
      department = department_fixture()
      assert {:error, %Ecto.Changeset{}} = Settings.update_department(department, @invalid_attrs)
      assert department == Settings.get_department!(department.id)
    end

    test "delete_department/1 deletes the department" do
      department = department_fixture()
      assert {:ok, %Department{}} = Settings.delete_department(department)
      assert_raise Ecto.NoResultsError, fn -> Settings.get_department!(department.id) end
    end

    test "change_department/1 returns a department changeset" do
      department = department_fixture()
      assert %Ecto.Changeset{} = Settings.change_department(department)
    end
  end

  describe "positions" do
    alias Portal.Settings.Position

    @valid_attrs %{position: "some position"}
    @update_attrs %{position: "some updated position"}
    @invalid_attrs %{position: nil}

    def position_fixture(attrs \\ %{}) do
      {:ok, position} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Settings.create_position()

      position
    end

    test "list_positions/0 returns all positions" do
      position = position_fixture()
      assert Settings.list_positions() == [position]
    end

    test "get_position!/1 returns the position with given id" do
      position = position_fixture()
      assert Settings.get_position!(position.id) == position
    end

    test "create_position/1 with valid data creates a position" do
      assert {:ok, %Position{} = position} = Settings.create_position(@valid_attrs)
      assert position.position == "some position"
    end

    test "create_position/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Settings.create_position(@invalid_attrs)
    end

    test "update_position/2 with valid data updates the position" do
      position = position_fixture()
      assert {:ok, %Position{} = position} = Settings.update_position(position, @update_attrs)
      assert position.position == "some updated position"
    end

    test "update_position/2 with invalid data returns error changeset" do
      position = position_fixture()
      assert {:error, %Ecto.Changeset{}} = Settings.update_position(position, @invalid_attrs)
      assert position == Settings.get_position!(position.id)
    end

    test "delete_position/1 deletes the position" do
      position = position_fixture()
      assert {:ok, %Position{}} = Settings.delete_position(position)
      assert_raise Ecto.NoResultsError, fn -> Settings.get_position!(position.id) end
    end

    test "change_position/1 returns a position changeset" do
      position = position_fixture()
      assert %Ecto.Changeset{} = Settings.change_position(position)
    end
  end

  describe "leaves" do
    alias Portal.Settings.Leave

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def leave_fixture(attrs \\ %{}) do
      {:ok, leave} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Settings.create_leave()

      leave
    end

    test "list_leaves/0 returns all leaves" do
      leave = leave_fixture()
      assert Settings.list_leaves() == [leave]
    end

    test "get_leave!/1 returns the leave with given id" do
      leave = leave_fixture()
      assert Settings.get_leave!(leave.id) == leave
    end

    test "create_leave/1 with valid data creates a leave" do
      assert {:ok, %Leave{} = leave} = Settings.create_leave(@valid_attrs)
      assert leave.name == "some name"
    end

    test "create_leave/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Settings.create_leave(@invalid_attrs)
    end

    test "update_leave/2 with valid data updates the leave" do
      leave = leave_fixture()
      assert {:ok, %Leave{} = leave} = Settings.update_leave(leave, @update_attrs)
      assert leave.name == "some updated name"
    end

    test "update_leave/2 with invalid data returns error changeset" do
      leave = leave_fixture()
      assert {:error, %Ecto.Changeset{}} = Settings.update_leave(leave, @invalid_attrs)
      assert leave == Settings.get_leave!(leave.id)
    end

    test "delete_leave/1 deletes the leave" do
      leave = leave_fixture()
      assert {:ok, %Leave{}} = Settings.delete_leave(leave)
      assert_raise Ecto.NoResultsError, fn -> Settings.get_leave!(leave.id) end
    end

    test "change_leave/1 returns a leave changeset" do
      leave = leave_fixture()
      assert %Ecto.Changeset{} = Settings.change_leave(leave)
    end
  end

  describe "loansets" do
    alias Portal.Settings.Loanset

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def loanset_fixture(attrs \\ %{}) do
      {:ok, loanset} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Settings.create_loanset()

      loanset
    end

    test "list_loansets/0 returns all loansets" do
      loanset = loanset_fixture()
      assert Settings.list_loansets() == [loanset]
    end

    test "get_loanset!/1 returns the loanset with given id" do
      loanset = loanset_fixture()
      assert Settings.get_loanset!(loanset.id) == loanset
    end

    test "create_loanset/1 with valid data creates a loanset" do
      assert {:ok, %Loanset{} = loanset} = Settings.create_loanset(@valid_attrs)
      assert loanset.name == "some name"
    end

    test "create_loanset/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Settings.create_loanset(@invalid_attrs)
    end

    test "update_loanset/2 with valid data updates the loanset" do
      loanset = loanset_fixture()
      assert {:ok, %Loanset{} = loanset} = Settings.update_loanset(loanset, @update_attrs)
      assert loanset.name == "some updated name"
    end

    test "update_loanset/2 with invalid data returns error changeset" do
      loanset = loanset_fixture()
      assert {:error, %Ecto.Changeset{}} = Settings.update_loanset(loanset, @invalid_attrs)
      assert loanset == Settings.get_loanset!(loanset.id)
    end

    test "delete_loanset/1 deletes the loanset" do
      loanset = loanset_fixture()
      assert {:ok, %Loanset{}} = Settings.delete_loanset(loanset)
      assert_raise Ecto.NoResultsError, fn -> Settings.get_loanset!(loanset.id) end
    end

    test "change_loanset/1 returns a loanset changeset" do
      loanset = loanset_fixture()
      assert %Ecto.Changeset{} = Settings.change_loanset(loanset)
    end
  end
end

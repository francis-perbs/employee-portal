defmodule Portal.DisbursementTest do
  use Portal.DataCase

  alias Portal.Disbursement

  describe "payrolls" do
    alias Portal.Disbursement.Payroll

    @valid_attrs %{absences: "120.5", allowances: "120.5", beginningDate: ~D[2010-04-17], bonus: "120.5", cashAdvances: "120.5", commission: "120.5", cutoffDate: ~D[2010-04-17], empCode: "some empCode", loan: "120.5", payrollCode: "some payrollCode", status: "some status", tardiness: "120.5", thirteenthMonth: "120.5"}
    @update_attrs %{absences: "456.7", allowances: "456.7", beginningDate: ~D[2011-05-18], bonus: "456.7", cashAdvances: "456.7", commission: "456.7", cutoffDate: ~D[2011-05-18], empCode: "some updated empCode", loan: "456.7", payrollCode: "some updated payrollCode", status: "some updated status", tardiness: "456.7", thirteenthMonth: "456.7"}
    @invalid_attrs %{absences: nil, allowances: nil, beginningDate: nil, bonus: nil, cashAdvances: nil, commission: nil, cutoffDate: nil, empCode: nil, loan: nil, payrollCode: nil, status: nil, tardiness: nil, thirteenthMonth: nil}

    def payroll_fixture(attrs \\ %{}) do
      {:ok, payroll} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Disbursement.create_payroll()

      payroll
    end

    test "list_payrolls/0 returns all payrolls" do
      payroll = payroll_fixture()
      assert Disbursement.list_payrolls() == [payroll]
    end

    test "get_payroll!/1 returns the payroll with given id" do
      payroll = payroll_fixture()
      assert Disbursement.get_payroll!(payroll.id) == payroll
    end

    test "create_payroll/1 with valid data creates a payroll" do
      assert {:ok, %Payroll{} = payroll} = Disbursement.create_payroll(@valid_attrs)
      assert payroll.absences == Decimal.new("120.5")
      assert payroll.allowances == Decimal.new("120.5")
      assert payroll.beginningDate == ~D[2010-04-17]
      assert payroll.bonus == Decimal.new("120.5")
      assert payroll.cashAdvances == Decimal.new("120.5")
      assert payroll.commission == Decimal.new("120.5")
      assert payroll.cutoffDate == ~D[2010-04-17]
      assert payroll.empCode == "some empCode"
      assert payroll.loan == Decimal.new("120.5")
      assert payroll.payrollCode == "some payrollCode"
      assert payroll.status == "some status"
      assert payroll.tardiness == Decimal.new("120.5")
      assert payroll.thirteenthMonth == Decimal.new("120.5")
    end

    test "create_payroll/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Disbursement.create_payroll(@invalid_attrs)
    end

    test "update_payroll/2 with valid data updates the payroll" do
      payroll = payroll_fixture()
      assert {:ok, %Payroll{} = payroll} = Disbursement.update_payroll(payroll, @update_attrs)
      assert payroll.absences == Decimal.new("456.7")
      assert payroll.allowances == Decimal.new("456.7")
      assert payroll.beginningDate == ~D[2011-05-18]
      assert payroll.bonus == Decimal.new("456.7")
      assert payroll.cashAdvances == Decimal.new("456.7")
      assert payroll.commission == Decimal.new("456.7")
      assert payroll.cutoffDate == ~D[2011-05-18]
      assert payroll.empCode == "some updated empCode"
      assert payroll.loan == Decimal.new("456.7")
      assert payroll.payrollCode == "some updated payrollCode"
      assert payroll.status == "some updated status"
      assert payroll.tardiness == Decimal.new("456.7")
      assert payroll.thirteenthMonth == Decimal.new("456.7")
    end

    test "update_payroll/2 with invalid data returns error changeset" do
      payroll = payroll_fixture()
      assert {:error, %Ecto.Changeset{}} = Disbursement.update_payroll(payroll, @invalid_attrs)
      assert payroll == Disbursement.get_payroll!(payroll.id)
    end

    test "delete_payroll/1 deletes the payroll" do
      payroll = payroll_fixture()
      assert {:ok, %Payroll{}} = Disbursement.delete_payroll(payroll)
      assert_raise Ecto.NoResultsError, fn -> Disbursement.get_payroll!(payroll.id) end
    end

    test "change_payroll/1 returns a payroll changeset" do
      payroll = payroll_fixture()
      assert %Ecto.Changeset{} = Disbursement.change_payroll(payroll)
    end
  end

  describe "loans" do
    alias Portal.Disbursement.Loan

    @valid_attrs %{monthlyInterestRate: "120.5", name: "some name", payableMonths: "120.5", principal: "120.5"}
    @update_attrs %{monthlyInterestRate: "456.7", name: "some updated name", payableMonths: "456.7", principal: "456.7"}
    @invalid_attrs %{monthlyInterestRate: nil, name: nil, payableMonths: nil, principal: nil}

    def loan_fixture(attrs \\ %{}) do
      {:ok, loan} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Disbursement.create_loan()

      loan
    end

    test "list_loans/0 returns all loans" do
      loan = loan_fixture()
      assert Disbursement.list_loans() == [loan]
    end

    test "get_loan!/1 returns the loan with given id" do
      loan = loan_fixture()
      assert Disbursement.get_loan!(loan.id) == loan
    end

    test "create_loan/1 with valid data creates a loan" do
      assert {:ok, %Loan{} = loan} = Disbursement.create_loan(@valid_attrs)
      assert loan.monthlyInterestRate == Decimal.new("120.5")
      assert loan.name == "some name"
      assert loan.payableMonths == Decimal.new("120.5")
      assert loan.principal == Decimal.new("120.5")
    end

    test "create_loan/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Disbursement.create_loan(@invalid_attrs)
    end

    test "update_loan/2 with valid data updates the loan" do
      loan = loan_fixture()
      assert {:ok, %Loan{} = loan} = Disbursement.update_loan(loan, @update_attrs)
      assert loan.monthlyInterestRate == Decimal.new("456.7")
      assert loan.name == "some updated name"
      assert loan.payableMonths == Decimal.new("456.7")
      assert loan.principal == Decimal.new("456.7")
    end

    test "update_loan/2 with invalid data returns error changeset" do
      loan = loan_fixture()
      assert {:error, %Ecto.Changeset{}} = Disbursement.update_loan(loan, @invalid_attrs)
      assert loan == Disbursement.get_loan!(loan.id)
    end

    test "delete_loan/1 deletes the loan" do
      loan = loan_fixture()
      assert {:ok, %Loan{}} = Disbursement.delete_loan(loan)
      assert_raise Ecto.NoResultsError, fn -> Disbursement.get_loan!(loan.id) end
    end

    test "change_loan/1 returns a loan changeset" do
      loan = loan_fixture()
      assert %Ecto.Changeset{} = Disbursement.change_loan(loan)
    end
  end

  describe "claims" do
    alias Portal.Disbursement.Claim

    @valid_attrs %{description: "some description", docDate: ~D[2010-04-17], empCode: "some empCode", expenseClaimDate: ~D[2010-04-17], expenseClaimNbr: "some expenseClaimNbr", status: "some status", totalAmt: "120.5"}
    @update_attrs %{description: "some updated description", docDate: ~D[2011-05-18], empCode: "some updated empCode", expenseClaimDate: ~D[2011-05-18], expenseClaimNbr: "some updated expenseClaimNbr", status: "some updated status", totalAmt: "456.7"}
    @invalid_attrs %{description: nil, docDate: nil, empCode: nil, expenseClaimDate: nil, expenseClaimNbr: nil, status: nil, totalAmt: nil}

    def claim_fixture(attrs \\ %{}) do
      {:ok, claim} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Disbursement.create_claim()

      claim
    end

    test "list_claims/0 returns all claims" do
      claim = claim_fixture()
      assert Disbursement.list_claims() == [claim]
    end

    test "get_claim!/1 returns the claim with given id" do
      claim = claim_fixture()
      assert Disbursement.get_claim!(claim.id) == claim
    end

    test "create_claim/1 with valid data creates a claim" do
      assert {:ok, %Claim{} = claim} = Disbursement.create_claim(@valid_attrs)
      assert claim.description == "some description"
      assert claim.docDate == ~D[2010-04-17]
      assert claim.empCode == "some empCode"
      assert claim.expenseClaimDate == ~D[2010-04-17]
      assert claim.expenseClaimNbr == "some expenseClaimNbr"
      assert claim.status == "some status"
      assert claim.totalAmt == Decimal.new("120.5")
    end

    test "create_claim/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Disbursement.create_claim(@invalid_attrs)
    end

    test "update_claim/2 with valid data updates the claim" do
      claim = claim_fixture()
      assert {:ok, %Claim{} = claim} = Disbursement.update_claim(claim, @update_attrs)
      assert claim.description == "some updated description"
      assert claim.docDate == ~D[2011-05-18]
      assert claim.empCode == "some updated empCode"
      assert claim.expenseClaimDate == ~D[2011-05-18]
      assert claim.expenseClaimNbr == "some updated expenseClaimNbr"
      assert claim.status == "some updated status"
      assert claim.totalAmt == Decimal.new("456.7")
    end

    test "update_claim/2 with invalid data returns error changeset" do
      claim = claim_fixture()
      assert {:error, %Ecto.Changeset{}} = Disbursement.update_claim(claim, @invalid_attrs)
      assert claim == Disbursement.get_claim!(claim.id)
    end

    test "delete_claim/1 deletes the claim" do
      claim = claim_fixture()
      assert {:ok, %Claim{}} = Disbursement.delete_claim(claim)
      assert_raise Ecto.NoResultsError, fn -> Disbursement.get_claim!(claim.id) end
    end

    test "change_claim/1 returns a claim changeset" do
      claim = claim_fixture()
      assert %Ecto.Changeset{} = Disbursement.change_claim(claim)
    end
  end

  describe "claim_details" do
    alias Portal.Disbursement.ClaimDetail

    @valid_attrs %{description: "some description", expenseClaimNbr: "some expenseClaimNbr", expenseType: "some expenseType", lineAmt: "120.5"}
    @update_attrs %{description: "some updated description", expenseClaimNbr: "some updated expenseClaimNbr", expenseType: "some updated expenseType", lineAmt: "456.7"}
    @invalid_attrs %{description: nil, expenseClaimNbr: nil, expenseType: nil, lineAmt: nil}

    def claim_detail_fixture(attrs \\ %{}) do
      {:ok, claim_detail} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Disbursement.create_claim_detail()

      claim_detail
    end

    test "list_claim_details/0 returns all claim_details" do
      claim_detail = claim_detail_fixture()
      assert Disbursement.list_claim_details() == [claim_detail]
    end

    test "get_claim_detail!/1 returns the claim_detail with given id" do
      claim_detail = claim_detail_fixture()
      assert Disbursement.get_claim_detail!(claim_detail.id) == claim_detail
    end

    test "create_claim_detail/1 with valid data creates a claim_detail" do
      assert {:ok, %ClaimDetail{} = claim_detail} = Disbursement.create_claim_detail(@valid_attrs)
      assert claim_detail.description == "some description"
      assert claim_detail.expenseClaimNbr == "some expenseClaimNbr"
      assert claim_detail.expenseType == "some expenseType"
      assert claim_detail.lineAmt == Decimal.new("120.5")
    end

    test "create_claim_detail/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Disbursement.create_claim_detail(@invalid_attrs)
    end

    test "update_claim_detail/2 with valid data updates the claim_detail" do
      claim_detail = claim_detail_fixture()
      assert {:ok, %ClaimDetail{} = claim_detail} = Disbursement.update_claim_detail(claim_detail, @update_attrs)
      assert claim_detail.description == "some updated description"
      assert claim_detail.expenseClaimNbr == "some updated expenseClaimNbr"
      assert claim_detail.expenseType == "some updated expenseType"
      assert claim_detail.lineAmt == Decimal.new("456.7")
    end

    test "update_claim_detail/2 with invalid data returns error changeset" do
      claim_detail = claim_detail_fixture()
      assert {:error, %Ecto.Changeset{}} = Disbursement.update_claim_detail(claim_detail, @invalid_attrs)
      assert claim_detail == Disbursement.get_claim_detail!(claim_detail.id)
    end

    test "delete_claim_detail/1 deletes the claim_detail" do
      claim_detail = claim_detail_fixture()
      assert {:ok, %ClaimDetail{}} = Disbursement.delete_claim_detail(claim_detail)
      assert_raise Ecto.NoResultsError, fn -> Disbursement.get_claim_detail!(claim_detail.id) end
    end

    test "change_claim_detail/1 returns a claim_detail changeset" do
      claim_detail = claim_detail_fixture()
      assert %Ecto.Changeset{} = Disbursement.change_claim_detail(claim_detail)
    end
  end
end

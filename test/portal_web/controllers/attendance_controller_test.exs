defmodule PortalWeb.AttendanceControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Timekeeping

  @create_attrs %{dateTimeIn: "2010-04-17T14:00:00Z", dateTimeOut: "2010-04-17T14:00:00Z", empCode: "some empCode", workHours: "120.5", workType: "some workType"}
  @update_attrs %{dateTimeIn: "2011-05-18T15:01:01Z", dateTimeOut: "2011-05-18T15:01:01Z", empCode: "some updated empCode", workHours: "456.7", workType: "some updated workType"}
  @invalid_attrs %{dateTimeIn: nil, dateTimeOut: nil, empCode: nil, workHours: nil, workType: nil}

  def fixture(:attendance) do
    {:ok, attendance} = Timekeeping.create_attendance(@create_attrs)
    attendance
  end

  describe "index" do
    test "lists all attendances", %{conn: conn} do
      conn = get(conn, Routes.attendance_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Attendances"
    end
  end

  describe "new attendance" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.attendance_path(conn, :new))
      assert html_response(conn, 200) =~ "New Attendance"
    end
  end

  describe "create attendance" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.attendance_path(conn, :create), attendance: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.attendance_path(conn, :show, id)

      conn = get(conn, Routes.attendance_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Attendance"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.attendance_path(conn, :create), attendance: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Attendance"
    end
  end

  describe "edit attendance" do
    setup [:create_attendance]

    test "renders form for editing chosen attendance", %{conn: conn, attendance: attendance} do
      conn = get(conn, Routes.attendance_path(conn, :edit, attendance))
      assert html_response(conn, 200) =~ "Edit Attendance"
    end
  end

  describe "update attendance" do
    setup [:create_attendance]

    test "redirects when data is valid", %{conn: conn, attendance: attendance} do
      conn = put(conn, Routes.attendance_path(conn, :update, attendance), attendance: @update_attrs)
      assert redirected_to(conn) == Routes.attendance_path(conn, :show, attendance)

      conn = get(conn, Routes.attendance_path(conn, :show, attendance))
      assert html_response(conn, 200) =~ "some updated empCode"
    end

    test "renders errors when data is invalid", %{conn: conn, attendance: attendance} do
      conn = put(conn, Routes.attendance_path(conn, :update, attendance), attendance: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Attendance"
    end
  end

  describe "delete attendance" do
    setup [:create_attendance]

    test "deletes chosen attendance", %{conn: conn, attendance: attendance} do
      conn = delete(conn, Routes.attendance_path(conn, :delete, attendance))
      assert redirected_to(conn) == Routes.attendance_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.attendance_path(conn, :show, attendance))
      end
    end
  end

  defp create_attendance(_) do
    attendance = fixture(:attendance)
    %{attendance: attendance}
  end
end

defmodule PortalWeb.EmpLeaveControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Profiles

  @create_attrs %{leaveQty: "120.5", leaveType: "some leaveType"}
  @update_attrs %{leaveQty: "456.7", leaveType: "some updated leaveType"}
  @invalid_attrs %{leaveQty: nil, leaveType: nil}

  def fixture(:emp_leave) do
    {:ok, emp_leave} = Profiles.create_emp_leave(@create_attrs)
    emp_leave
  end

  describe "index" do
    test "lists all emp_leaves", %{conn: conn} do
      conn = get(conn, Routes.emp_leave_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Emp leaves"
    end
  end

  describe "new emp_leave" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.emp_leave_path(conn, :new))
      assert html_response(conn, 200) =~ "New Emp leave"
    end
  end

  describe "create emp_leave" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.emp_leave_path(conn, :create), emp_leave: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.emp_leave_path(conn, :show, id)

      conn = get(conn, Routes.emp_leave_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Emp leave"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.emp_leave_path(conn, :create), emp_leave: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Emp leave"
    end
  end

  describe "edit emp_leave" do
    setup [:create_emp_leave]

    test "renders form for editing chosen emp_leave", %{conn: conn, emp_leave: emp_leave} do
      conn = get(conn, Routes.emp_leave_path(conn, :edit, emp_leave))
      assert html_response(conn, 200) =~ "Edit Emp leave"
    end
  end

  describe "update emp_leave" do
    setup [:create_emp_leave]

    test "redirects when data is valid", %{conn: conn, emp_leave: emp_leave} do
      conn = put(conn, Routes.emp_leave_path(conn, :update, emp_leave), emp_leave: @update_attrs)
      assert redirected_to(conn) == Routes.emp_leave_path(conn, :show, emp_leave)

      conn = get(conn, Routes.emp_leave_path(conn, :show, emp_leave))
      assert html_response(conn, 200) =~ "some updated leaveType"
    end

    test "renders errors when data is invalid", %{conn: conn, emp_leave: emp_leave} do
      conn = put(conn, Routes.emp_leave_path(conn, :update, emp_leave), emp_leave: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Emp leave"
    end
  end

  describe "delete emp_leave" do
    setup [:create_emp_leave]

    test "deletes chosen emp_leave", %{conn: conn, emp_leave: emp_leave} do
      conn = delete(conn, Routes.emp_leave_path(conn, :delete, emp_leave))
      assert redirected_to(conn) == Routes.emp_leave_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.emp_leave_path(conn, :show, emp_leave))
      end
    end
  end

  defp create_emp_leave(_) do
    emp_leave = fixture(:emp_leave)
    %{emp_leave: emp_leave}
  end
end

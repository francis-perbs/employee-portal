defmodule PortalWeb.EmployeeControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Profiles

  @create_attrs %{allowance: "120.5", bankAcctNbr: "some bankAcctNbr", basicSalary: "120.5", birthDate: ~D[2010-04-17], contactNbr: "some contactNbr", countryID: 42, departmentID: 42, email: "some email", employeeCode: "some employeeCode", employeeStatus: "some employeeStatus", firstName: "some firstName", gender: "some gender", hdmfNbr: "some hdmfNbr", hdmfPrem: "120.5", hireDate: ~D[2010-04-17], hmoNbr: "some hmoNbr", jobTitleID: 42, lastName: "some lastName", middleName: "some middleName", permanentAddr: "some permanentAddr", philhealthNbr: "some philhealthNbr", philhealthPrem: "120.5", presentAddr: "some presentAddr", sssNbr: "some sssNbr", sssPrem: "120.5", status: "some status", taxStatus: "some taxStatus", taxWth: "120.5", terminationDate: ~D[2010-04-17], tinNbr: "some tinNbr"}
  @update_attrs %{allowance: "456.7", bankAcctNbr: "some updated bankAcctNbr", basicSalary: "456.7", birthDate: ~D[2011-05-18], contactNbr: "some updated contactNbr", countryID: 43, departmentID: 43, email: "some updated email", employeeCode: "some updated employeeCode", employeeStatus: "some updated employeeStatus", firstName: "some updated firstName", gender: "some updated gender", hdmfNbr: "some updated hdmfNbr", hdmfPrem: "456.7", hireDate: ~D[2011-05-18], hmoNbr: "some updated hmoNbr", jobTitleID: 43, lastName: "some updated lastName", middleName: "some updated middleName", permanentAddr: "some updated permanentAddr", philhealthNbr: "some updated philhealthNbr", philhealthPrem: "456.7", presentAddr: "some updated presentAddr", sssNbr: "some updated sssNbr", sssPrem: "456.7", status: "some updated status", taxStatus: "some updated taxStatus", taxWth: "456.7", terminationDate: ~D[2011-05-18], tinNbr: "some updated tinNbr"}
  @invalid_attrs %{allowance: nil, bankAcctNbr: nil, basicSalary: nil, birthDate: nil, contactNbr: nil, countryID: nil, departmentID: nil, email: nil, employeeCode: nil, employeeStatus: nil, firstName: nil, gender: nil, hdmfNbr: nil, hdmfPrem: nil, hireDate: nil, hmoNbr: nil, jobTitleID: nil, lastName: nil, middleName: nil, permanentAddr: nil, philhealthNbr: nil, philhealthPrem: nil, presentAddr: nil, sssNbr: nil, sssPrem: nil, status: nil, taxStatus: nil, taxWth: nil, terminationDate: nil, tinNbr: nil}

  def fixture(:employee) do
    {:ok, employee} = Profiles.create_employee(@create_attrs)
    employee
  end

  describe "index" do
    test "lists all employees", %{conn: conn} do
      conn = get(conn, Routes.employee_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Employees"
    end
  end

  describe "new employee" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.employee_path(conn, :new))
      assert html_response(conn, 200) =~ "New Employee"
    end
  end

  describe "create employee" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.employee_path(conn, :create), employee: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.employee_path(conn, :show, id)

      conn = get(conn, Routes.employee_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Employee"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.employee_path(conn, :create), employee: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Employee"
    end
  end

  describe "edit employee" do
    setup [:create_employee]

    test "renders form for editing chosen employee", %{conn: conn, employee: employee} do
      conn = get(conn, Routes.employee_path(conn, :edit, employee))
      assert html_response(conn, 200) =~ "Edit Employee"
    end
  end

  describe "update employee" do
    setup [:create_employee]

    test "redirects when data is valid", %{conn: conn, employee: employee} do
      conn = put(conn, Routes.employee_path(conn, :update, employee), employee: @update_attrs)
      assert redirected_to(conn) == Routes.employee_path(conn, :show, employee)

      conn = get(conn, Routes.employee_path(conn, :show, employee))
      assert html_response(conn, 200) =~ "some updated bankAcctNbr"
    end

    test "renders errors when data is invalid", %{conn: conn, employee: employee} do
      conn = put(conn, Routes.employee_path(conn, :update, employee), employee: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Employee"
    end
  end

  describe "delete employee" do
    setup [:create_employee]

    test "deletes chosen employee", %{conn: conn, employee: employee} do
      conn = delete(conn, Routes.employee_path(conn, :delete, employee))
      assert redirected_to(conn) == Routes.employee_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.employee_path(conn, :show, employee))
      end
    end
  end

  defp create_employee(_) do
    employee = fixture(:employee)
    %{employee: employee}
  end
end

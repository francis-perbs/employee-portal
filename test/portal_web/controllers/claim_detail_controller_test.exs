defmodule PortalWeb.ClaimDetailControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Disbursement

  @create_attrs %{description: "some description", expenseClaimNbr: "some expenseClaimNbr", expenseType: "some expenseType", lineAmt: "120.5"}
  @update_attrs %{description: "some updated description", expenseClaimNbr: "some updated expenseClaimNbr", expenseType: "some updated expenseType", lineAmt: "456.7"}
  @invalid_attrs %{description: nil, expenseClaimNbr: nil, expenseType: nil, lineAmt: nil}

  def fixture(:claim_detail) do
    {:ok, claim_detail} = Disbursement.create_claim_detail(@create_attrs)
    claim_detail
  end

  describe "index" do
    test "lists all claim_details", %{conn: conn} do
      conn = get(conn, Routes.claim_detail_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Claim details"
    end
  end

  describe "new claim_detail" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.claim_detail_path(conn, :new))
      assert html_response(conn, 200) =~ "New Claim detail"
    end
  end

  describe "create claim_detail" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.claim_detail_path(conn, :create), claim_detail: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.claim_detail_path(conn, :show, id)

      conn = get(conn, Routes.claim_detail_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Claim detail"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.claim_detail_path(conn, :create), claim_detail: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Claim detail"
    end
  end

  describe "edit claim_detail" do
    setup [:create_claim_detail]

    test "renders form for editing chosen claim_detail", %{conn: conn, claim_detail: claim_detail} do
      conn = get(conn, Routes.claim_detail_path(conn, :edit, claim_detail))
      assert html_response(conn, 200) =~ "Edit Claim detail"
    end
  end

  describe "update claim_detail" do
    setup [:create_claim_detail]

    test "redirects when data is valid", %{conn: conn, claim_detail: claim_detail} do
      conn = put(conn, Routes.claim_detail_path(conn, :update, claim_detail), claim_detail: @update_attrs)
      assert redirected_to(conn) == Routes.claim_detail_path(conn, :show, claim_detail)

      conn = get(conn, Routes.claim_detail_path(conn, :show, claim_detail))
      assert html_response(conn, 200) =~ "some updated description"
    end

    test "renders errors when data is invalid", %{conn: conn, claim_detail: claim_detail} do
      conn = put(conn, Routes.claim_detail_path(conn, :update, claim_detail), claim_detail: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Claim detail"
    end
  end

  describe "delete claim_detail" do
    setup [:create_claim_detail]

    test "deletes chosen claim_detail", %{conn: conn, claim_detail: claim_detail} do
      conn = delete(conn, Routes.claim_detail_path(conn, :delete, claim_detail))
      assert redirected_to(conn) == Routes.claim_detail_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.claim_detail_path(conn, :show, claim_detail))
      end
    end
  end

  defp create_claim_detail(_) do
    claim_detail = fixture(:claim_detail)
    %{claim_detail: claim_detail}
  end
end

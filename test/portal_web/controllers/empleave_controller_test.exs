defmodule PortalWeb.EmpleaveControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Profiles

  @create_attrs %{leaveQty: "120.5", leaveType: "some leaveType"}
  @update_attrs %{leaveQty: "456.7", leaveType: "some updated leaveType"}
  @invalid_attrs %{leaveQty: nil, leaveType: nil}

  def fixture(:empleave) do
    {:ok, empleave} = Profiles.create_empleave(@create_attrs)
    empleave
  end

  describe "index" do
    test "lists all emp_leaves", %{conn: conn} do
      conn = get(conn, Routes.empleave_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Emp leaves"
    end
  end

  describe "new empleave" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.empleave_path(conn, :new))
      assert html_response(conn, 200) =~ "New Empleave"
    end
  end

  describe "create empleave" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.empleave_path(conn, :create), empleave: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.empleave_path(conn, :show, id)

      conn = get(conn, Routes.empleave_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Empleave"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.empleave_path(conn, :create), empleave: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Empleave"
    end
  end

  describe "edit empleave" do
    setup [:create_empleave]

    test "renders form for editing chosen empleave", %{conn: conn, empleave: empleave} do
      conn = get(conn, Routes.empleave_path(conn, :edit, empleave))
      assert html_response(conn, 200) =~ "Edit Empleave"
    end
  end

  describe "update empleave" do
    setup [:create_empleave]

    test "redirects when data is valid", %{conn: conn, empleave: empleave} do
      conn = put(conn, Routes.empleave_path(conn, :update, empleave), empleave: @update_attrs)
      assert redirected_to(conn) == Routes.empleave_path(conn, :show, empleave)

      conn = get(conn, Routes.empleave_path(conn, :show, empleave))
      assert html_response(conn, 200) =~ "some updated leaveType"
    end

    test "renders errors when data is invalid", %{conn: conn, empleave: empleave} do
      conn = put(conn, Routes.empleave_path(conn, :update, empleave), empleave: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Empleave"
    end
  end

  describe "delete empleave" do
    setup [:create_empleave]

    test "deletes chosen empleave", %{conn: conn, empleave: empleave} do
      conn = delete(conn, Routes.empleave_path(conn, :delete, empleave))
      assert redirected_to(conn) == Routes.empleave_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.empleave_path(conn, :show, empleave))
      end
    end
  end

  defp create_empleave(_) do
    empleave = fixture(:empleave)
    %{empleave: empleave}
  end
end

defmodule PortalWeb.EmpLoanControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Profiles

  @create_attrs %{loanDate: ~D[2010-04-17], loanType: "some loanType", payableMonths: "120.5", principalAmt: "120.5"}
  @update_attrs %{loanDate: ~D[2011-05-18], loanType: "some updated loanType", payableMonths: "456.7", principalAmt: "456.7"}
  @invalid_attrs %{loanDate: nil, loanType: nil, payableMonths: nil, principalAmt: nil}

  def fixture(:emp_loan) do
    {:ok, emp_loan} = Profiles.create_emp_loan(@create_attrs)
    emp_loan
  end

  describe "index" do
    test "lists all emp_loans", %{conn: conn} do
      conn = get(conn, Routes.emp_loan_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Emp loans"
    end
  end

  describe "new emp_loan" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.emp_loan_path(conn, :new))
      assert html_response(conn, 200) =~ "New Emp loan"
    end
  end

  describe "create emp_loan" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.emp_loan_path(conn, :create), emp_loan: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.emp_loan_path(conn, :show, id)

      conn = get(conn, Routes.emp_loan_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Emp loan"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.emp_loan_path(conn, :create), emp_loan: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Emp loan"
    end
  end

  describe "edit emp_loan" do
    setup [:create_emp_loan]

    test "renders form for editing chosen emp_loan", %{conn: conn, emp_loan: emp_loan} do
      conn = get(conn, Routes.emp_loan_path(conn, :edit, emp_loan))
      assert html_response(conn, 200) =~ "Edit Emp loan"
    end
  end

  describe "update emp_loan" do
    setup [:create_emp_loan]

    test "redirects when data is valid", %{conn: conn, emp_loan: emp_loan} do
      conn = put(conn, Routes.emp_loan_path(conn, :update, emp_loan), emp_loan: @update_attrs)
      assert redirected_to(conn) == Routes.emp_loan_path(conn, :show, emp_loan)

      conn = get(conn, Routes.emp_loan_path(conn, :show, emp_loan))
      assert html_response(conn, 200) =~ "some updated loanType"
    end

    test "renders errors when data is invalid", %{conn: conn, emp_loan: emp_loan} do
      conn = put(conn, Routes.emp_loan_path(conn, :update, emp_loan), emp_loan: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Emp loan"
    end
  end

  describe "delete emp_loan" do
    setup [:create_emp_loan]

    test "deletes chosen emp_loan", %{conn: conn, emp_loan: emp_loan} do
      conn = delete(conn, Routes.emp_loan_path(conn, :delete, emp_loan))
      assert redirected_to(conn) == Routes.emp_loan_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.emp_loan_path(conn, :show, emp_loan))
      end
    end
  end

  defp create_emp_loan(_) do
    emp_loan = fixture(:emp_loan)
    %{emp_loan: emp_loan}
  end
end

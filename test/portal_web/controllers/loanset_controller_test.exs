defmodule PortalWeb.LoansetControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Settings

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  def fixture(:loanset) do
    {:ok, loanset} = Settings.create_loanset(@create_attrs)
    loanset
  end

  describe "index" do
    test "lists all loansets", %{conn: conn} do
      conn = get(conn, Routes.loanset_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Loansets"
    end
  end

  describe "new loanset" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.loanset_path(conn, :new))
      assert html_response(conn, 200) =~ "New Loanset"
    end
  end

  describe "create loanset" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.loanset_path(conn, :create), loanset: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.loanset_path(conn, :show, id)

      conn = get(conn, Routes.loanset_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Loanset"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.loanset_path(conn, :create), loanset: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Loanset"
    end
  end

  describe "edit loanset" do
    setup [:create_loanset]

    test "renders form for editing chosen loanset", %{conn: conn, loanset: loanset} do
      conn = get(conn, Routes.loanset_path(conn, :edit, loanset))
      assert html_response(conn, 200) =~ "Edit Loanset"
    end
  end

  describe "update loanset" do
    setup [:create_loanset]

    test "redirects when data is valid", %{conn: conn, loanset: loanset} do
      conn = put(conn, Routes.loanset_path(conn, :update, loanset), loanset: @update_attrs)
      assert redirected_to(conn) == Routes.loanset_path(conn, :show, loanset)

      conn = get(conn, Routes.loanset_path(conn, :show, loanset))
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, loanset: loanset} do
      conn = put(conn, Routes.loanset_path(conn, :update, loanset), loanset: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Loanset"
    end
  end

  describe "delete loanset" do
    setup [:create_loanset]

    test "deletes chosen loanset", %{conn: conn, loanset: loanset} do
      conn = delete(conn, Routes.loanset_path(conn, :delete, loanset))
      assert redirected_to(conn) == Routes.loanset_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.loanset_path(conn, :show, loanset))
      end
    end
  end

  defp create_loanset(_) do
    loanset = fixture(:loanset)
    %{loanset: loanset}
  end
end

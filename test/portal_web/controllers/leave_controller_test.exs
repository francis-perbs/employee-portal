defmodule PortalWeb.LeaveControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Settings

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  def fixture(:leave) do
    {:ok, leave} = Settings.create_leave(@create_attrs)
    leave
  end

  describe "index" do
    test "lists all leaves", %{conn: conn} do
      conn = get(conn, Routes.leave_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Leaves"
    end
  end

  describe "new leave" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.leave_path(conn, :new))
      assert html_response(conn, 200) =~ "New Leave"
    end
  end

  describe "create leave" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.leave_path(conn, :create), leave: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.leave_path(conn, :show, id)

      conn = get(conn, Routes.leave_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Leave"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.leave_path(conn, :create), leave: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Leave"
    end
  end

  describe "edit leave" do
    setup [:create_leave]

    test "renders form for editing chosen leave", %{conn: conn, leave: leave} do
      conn = get(conn, Routes.leave_path(conn, :edit, leave))
      assert html_response(conn, 200) =~ "Edit Leave"
    end
  end

  describe "update leave" do
    setup [:create_leave]

    test "redirects when data is valid", %{conn: conn, leave: leave} do
      conn = put(conn, Routes.leave_path(conn, :update, leave), leave: @update_attrs)
      assert redirected_to(conn) == Routes.leave_path(conn, :show, leave)

      conn = get(conn, Routes.leave_path(conn, :show, leave))
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, leave: leave} do
      conn = put(conn, Routes.leave_path(conn, :update, leave), leave: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Leave"
    end
  end

  describe "delete leave" do
    setup [:create_leave]

    test "deletes chosen leave", %{conn: conn, leave: leave} do
      conn = delete(conn, Routes.leave_path(conn, :delete, leave))
      assert redirected_to(conn) == Routes.leave_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.leave_path(conn, :show, leave))
      end
    end
  end

  defp create_leave(_) do
    leave = fixture(:leave)
    %{leave: leave}
  end
end

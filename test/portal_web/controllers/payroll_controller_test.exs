defmodule PortalWeb.PayrollControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Disbursement

  @create_attrs %{absences: "120.5", allowances: "120.5", beginningDate: ~D[2010-04-17], bonus: "120.5", cashAdvances: "120.5", commission: "120.5", cutoffDate: ~D[2010-04-17], empCode: "some empCode", loan: "120.5", payrollCode: "some payrollCode", status: "some status", tardiness: "120.5", thirteenthMonth: "120.5"}
  @update_attrs %{absences: "456.7", allowances: "456.7", beginningDate: ~D[2011-05-18], bonus: "456.7", cashAdvances: "456.7", commission: "456.7", cutoffDate: ~D[2011-05-18], empCode: "some updated empCode", loan: "456.7", payrollCode: "some updated payrollCode", status: "some updated status", tardiness: "456.7", thirteenthMonth: "456.7"}
  @invalid_attrs %{absences: nil, allowances: nil, beginningDate: nil, bonus: nil, cashAdvances: nil, commission: nil, cutoffDate: nil, empCode: nil, loan: nil, payrollCode: nil, status: nil, tardiness: nil, thirteenthMonth: nil}

  def fixture(:payroll) do
    {:ok, payroll} = Disbursement.create_payroll(@create_attrs)
    payroll
  end

  describe "index" do
    test "lists all payrolls", %{conn: conn} do
      conn = get(conn, Routes.payroll_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Payrolls"
    end
  end

  describe "new payroll" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.payroll_path(conn, :new))
      assert html_response(conn, 200) =~ "New Payroll"
    end
  end

  describe "create payroll" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.payroll_path(conn, :create), payroll: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.payroll_path(conn, :show, id)

      conn = get(conn, Routes.payroll_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Payroll"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.payroll_path(conn, :create), payroll: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Payroll"
    end
  end

  describe "edit payroll" do
    setup [:create_payroll]

    test "renders form for editing chosen payroll", %{conn: conn, payroll: payroll} do
      conn = get(conn, Routes.payroll_path(conn, :edit, payroll))
      assert html_response(conn, 200) =~ "Edit Payroll"
    end
  end

  describe "update payroll" do
    setup [:create_payroll]

    test "redirects when data is valid", %{conn: conn, payroll: payroll} do
      conn = put(conn, Routes.payroll_path(conn, :update, payroll), payroll: @update_attrs)
      assert redirected_to(conn) == Routes.payroll_path(conn, :show, payroll)

      conn = get(conn, Routes.payroll_path(conn, :show, payroll))
      assert html_response(conn, 200) =~ "some updated empCode"
    end

    test "renders errors when data is invalid", %{conn: conn, payroll: payroll} do
      conn = put(conn, Routes.payroll_path(conn, :update, payroll), payroll: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Payroll"
    end
  end

  describe "delete payroll" do
    setup [:create_payroll]

    test "deletes chosen payroll", %{conn: conn, payroll: payroll} do
      conn = delete(conn, Routes.payroll_path(conn, :delete, payroll))
      assert redirected_to(conn) == Routes.payroll_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.payroll_path(conn, :show, payroll))
      end
    end
  end

  defp create_payroll(_) do
    payroll = fixture(:payroll)
    %{payroll: payroll}
  end
end

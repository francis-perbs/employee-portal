defmodule PortalWeb.EmploanControllerTest do
  use PortalWeb.ConnCase

  alias Portal.Profiles

  @create_attrs %{loanDate: ~D[2010-04-17], loanID: 42, loanName: "some loanName", payableMonths: "120.5", principalAmount: "120.5"}
  @update_attrs %{loanDate: ~D[2011-05-18], loanID: 43, loanName: "some updated loanName", payableMonths: "456.7", principalAmount: "456.7"}
  @invalid_attrs %{loanDate: nil, loanID: nil, loanName: nil, payableMonths: nil, principalAmount: nil}

  def fixture(:emploan) do
    {:ok, emploan} = Profiles.create_emploan(@create_attrs)
    emploan
  end

  describe "index" do
    test "lists all emp_loans", %{conn: conn} do
      conn = get(conn, Routes.emploan_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Emp loans"
    end
  end

  describe "new emploan" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.emploan_path(conn, :new))
      assert html_response(conn, 200) =~ "New Emploan"
    end
  end

  describe "create emploan" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.emploan_path(conn, :create), emploan: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.emploan_path(conn, :show, id)

      conn = get(conn, Routes.emploan_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Emploan"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.emploan_path(conn, :create), emploan: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Emploan"
    end
  end

  describe "edit emploan" do
    setup [:create_emploan]

    test "renders form for editing chosen emploan", %{conn: conn, emploan: emploan} do
      conn = get(conn, Routes.emploan_path(conn, :edit, emploan))
      assert html_response(conn, 200) =~ "Edit Emploan"
    end
  end

  describe "update emploan" do
    setup [:create_emploan]

    test "redirects when data is valid", %{conn: conn, emploan: emploan} do
      conn = put(conn, Routes.emploan_path(conn, :update, emploan), emploan: @update_attrs)
      assert redirected_to(conn) == Routes.emploan_path(conn, :show, emploan)

      conn = get(conn, Routes.emploan_path(conn, :show, emploan))
      assert html_response(conn, 200) =~ "some updated loanName"
    end

    test "renders errors when data is invalid", %{conn: conn, emploan: emploan} do
      conn = put(conn, Routes.emploan_path(conn, :update, emploan), emploan: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Emploan"
    end
  end

  describe "delete emploan" do
    setup [:create_emploan]

    test "deletes chosen emploan", %{conn: conn, emploan: emploan} do
      conn = delete(conn, Routes.emploan_path(conn, :delete, emploan))
      assert redirected_to(conn) == Routes.emploan_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.emploan_path(conn, :show, emploan))
      end
    end
  end

  defp create_emploan(_) do
    emploan = fixture(:emploan)
    %{emploan: emploan}
  end
end

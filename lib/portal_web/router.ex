defmodule PortalWeb.Router do
  use PortalWeb, :router
  use Pow.Phoenix.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :protected do
    plug Pow.Plug.RequireAuthenticated,
      error_handler: PortalWeb.AuthErrorHandler
  end

  pipeline :not_authenticated do
    plug Pow.Plug.RequireNotAuthenticated,
      error_handler: PortalWeb.AuthErrorHandler
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/" do
    pipe_through :browser

    pow_routes()
  end

  scope "/", PortalWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/", PortalWeb do
    pipe_through [:browser, :not_authenticated]

    get "/signup", RegistrationController, :new, as: :signup
    post "/signup", RegistrationController, :create, as: :signup
    get "/login", SessionController, :new, as: :login
    post "/login", SessionController, :create, as: :login
  end

  scope "/", PortalWeb do
    pipe_through [:browser, :protected]

    delete "/logout", SessionController, :delete, as: :logout
  end

  scope "/", PortalWeb do
    pipe_through [:browser, :protected]

    resources "/countries", CountryController
    resources "/departments", DepartmentController
    resources "/employees", EmployeeController do
      resources "/emp_leaves", EmpleaveController
      resources "/emp_loans", EmploanController
    end
    resources "/payrolls", PayrollController
    resources "/loans", LoanController
    resources "/positions", PositionController
    resources "/claims", ClaimController
    resources "/claim_details", ClaimDetailController
    resources "/leaves", LeaveController
    resources "/loansets", LoansetController
    resources "/attendances", AttendanceController
  end

  # Other scopes may use custom stacks.
  # scope "/api", PortalWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: PortalWeb.Telemetry
    end
  end
end

defmodule PortalWeb.AttendanceView do
  use PortalWeb, :view

  alias Portal.Profiles

  # def format_display(datetime) do
  #   Calendar.strftime(datetime, "%b-%d-%Y %I:%M:%S %p")
  # end

  # def datetime_local_input_format(datetime) do
  #   if datetime do
  #     Calendar.strftime(datetime, "%Y-%m-%dT%H:%M")
  #   end
  # end

  def format_time_display(datetime) do
    Calendar.strftime(datetime, "%I:%M %p")
  end

  def format_date_display(datetime) do
    Calendar.strftime(datetime, "%d/%m/%Y")
  end

  def get_employee_code(id) do
    employee = Profiles.get_employee!(id)
    employee.employeeCode
  end

  def get_employee_name(id) do
    employee = Profiles.get_employee!(id)
    employee.lastName <> ", " <> employee.firstName
  end
end

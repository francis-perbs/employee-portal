defmodule PortalWeb.ClaimView do
  use PortalWeb, :view

  def format_display(datetime) do
    Calendar.strftime(datetime, "%b-%d-%Y")
  end
end

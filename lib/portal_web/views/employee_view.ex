defmodule PortalWeb.EmployeeView do
  use PortalWeb, :view

  alias Portal.Settings

  def get_position_name(id) do
    position = Settings.get_position!(id)
    position.position
  end

  def get_dept_name(id) do
    dept = Settings.get_department!(id)
    dept.departmentName
  end

  def get_tax_name(employeeStatus) do
    case employeeStatus do
      "R" -> "Regular"
      "P" -> "Probationary"
      "T" -> "Resigned"
      "A" -> "AWOL"
      "V" -> "On Vacation"
       _  -> "Not Applicable"
    end
  end

end

defmodule PortalWeb.EmploanController do
  use PortalWeb, :controller

  alias Portal.Settings
  alias Portal.Profiles
  alias Portal.Profiles.Emploan

  def load_selections() do
    loans = Settings.list_loansets()
    %{ loans: loans }
  end

  def index(conn, _params) do
    emp_loans = Profiles.list_emp_loans()
    render(conn, "index.html", emp_loans: emp_loans)
  end

  def new(conn, %{"employee_id" => employee_id}) do
    emploan = %Emploan{employee_id: employee_id}
    changeset = Profiles.change_emploan(%Emploan{}, %{employee_id: employee_id})
    map_selections = load_selections()
    render(conn, "new.html", changeset: changeset, emploan: emploan, map_selections: map_selections)
  end

  def create(conn, %{"employee_id" => employee_id, "emploan" => emploan_params}) do
    case Profiles.create_emploan(emploan_params) do
      {:ok, _emploan} ->
        conn
        |> put_flash(:info, "Employee Loan created successfully.")
        |> redirect(to: Routes.employee_path(conn, :edit, employee_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Employee Loan not added")
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    emploan = Profiles.get_emploan!(id)
    map_selections = load_selections()
    render(conn, "show.html", emploan: emploan, map_selections: map_selections)
  end

  def edit(conn, %{"employee_id" => _employee_id, "id" => id}) do
    emploan = Profiles.get_emploan!(id)
    changeset = Profiles.change_emploan(emploan)
    map_selections = load_selections()
    render(conn, "edit.html", emploan: emploan, changeset: changeset, map_selections: map_selections)
  end

  def update(conn, %{"employee_id" => employee_id, "id" => id, "emploan" => emploan_params}) do
    emploan = Profiles.get_emploan!(id)

    case Profiles.update_emploan(emploan, emploan_params) do
      {:ok, _emploan} ->
        conn
        |> put_flash(:info, "Employee Loan updated successfully.")
        |> redirect(to: Routes.employee_path(conn, :edit, employee_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Employee Loan not added.")
        render(conn, "edit.html", emploan: emploan, changeset: changeset)
    end
  end

  @spec delete(Plug.Conn.t(), map) :: Plug.Conn.t()
  def delete(conn, %{"employee_id" => employee_id, "id" => id}) do
    emploan = Profiles.get_emploan!(id)
    {:ok, _emploan} = Profiles.delete_emploan(emploan)

    conn
    |> put_flash(:info, "Employee Loan deleted successfully.")
    |> redirect(to: Routes.employee_path(conn, :edit, employee_id))
  end
end

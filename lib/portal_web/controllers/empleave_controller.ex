defmodule PortalWeb.EmpleaveController do
  use PortalWeb, :controller

  alias Portal.Settings
  alias Portal.Profiles
  alias Portal.Profiles.Empleave

  def load_selections() do
    leaves = Settings.list_leaves()
    %{ leaves: leaves }
  end

  def index(conn, _params) do
    emp_leaves = Profiles.list_emp_leaves()
    render(conn, "index.html", emp_leaves: emp_leaves)
  end

  def new(conn, %{"employee_id" => employee_id}) do
    empleave = %Empleave{employee_id: employee_id}
    changeset = Profiles.change_empleave(%Empleave{}, %{employee_id: employee_id})
    map_selections = load_selections()
    render(conn, "new.html", changeset: changeset, empleave: empleave, map_selections: map_selections)
  end

  def create(conn, %{"employee_id" => employee_id, "empleave" => empleave_params}) do
    case Profiles.create_empleave(empleave_params) do
      {:ok, _empleave} ->
        conn
        |> put_flash(:info, "Employee Leave created successfully.")
        |> redirect(to: Routes.employee_path(conn, :edit, employee_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Employee Leave not added")
        render(conn, "new.html", changeset: changeset)
      # {:error, _error} ->
      #   conn
      #   |> put_flash(:error, "Employee Leave not added")
      #   |> redirect(to: Routes.employee_path(conn, :edit, employee_id))
    end
  end

  def show(conn, %{"id" => id}) do
    empleave = Profiles.get_empleave!(id)
    map_selections = load_selections()
    render(conn, "show.html", empleave: empleave, map_selections: map_selections)
  end

  def edit(conn, %{"employee_id" => _employee_id, "id" => id}) do
    empleave = Profiles.get_empleave!(id)
    changeset = Profiles.change_empleave(empleave)
    map_selections = load_selections()
    render(conn, "edit.html", empleave: empleave, changeset: changeset, map_selections: map_selections)
  end

  def update(conn, %{"employee_id" => employee_id, "id" => id, "empleave" => empleave_params}) do
    empleave = Profiles.get_empleave!(id)

    case Profiles.update_empleave(empleave, empleave_params) do
      {:ok, _empleave} ->
        conn
        |> put_flash(:info, "Employee Leave updated successfully.")
        |> redirect(to: Routes.employee_path(conn, :edit, employee_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Employee Leave not added.")
        render(conn, "edit.html", empleave: empleave, changeset: changeset)
    end
  end

  def delete(conn, %{"employee_id" => employee_id, "id" => id}) do
    empleave = Profiles.get_empleave!(id)
    {:ok, _empleave} = Profiles.delete_empleave(empleave)

    conn
    |> put_flash(:info, "Employee Leave deleted successfully.")
    |> redirect(to: Routes.employee_path(conn, :edit, employee_id))
  end
end

defmodule PortalWeb.PayrollController do
  use PortalWeb, :controller

  alias Portal.Disbursement
  alias Portal.Disbursement.Payroll

  def index(conn, _params) do
    payrolls = Disbursement.list_payrolls()
    render(conn, "index.html", payrolls: payrolls)
  end

  def new(conn, _params) do
    changeset = Disbursement.change_payroll(%Payroll{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"payroll" => payroll_params}) do
    case Disbursement.create_payroll(payroll_params) do
      {:ok, payroll} ->
        conn
        |> put_flash(:info, "Payroll created successfully.")
        |> redirect(to: Routes.payroll_path(conn, :show, payroll))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    payroll = Disbursement.get_payroll!(id)
    render(conn, "show.html", payroll: payroll)
  end

  def edit(conn, %{"id" => id}) do
    payroll = Disbursement.get_payroll!(id)
    changeset = Disbursement.change_payroll(payroll)
    render(conn, "edit.html", payroll: payroll, changeset: changeset)
  end

  def update(conn, %{"id" => id, "payroll" => payroll_params}) do
    payroll = Disbursement.get_payroll!(id)

    case Disbursement.update_payroll(payroll, payroll_params) do
      {:ok, payroll} ->
        conn
        |> put_flash(:info, "Payroll updated successfully.")
        |> redirect(to: Routes.payroll_path(conn, :show, payroll))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", payroll: payroll, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    payroll = Disbursement.get_payroll!(id)
    {:ok, _payroll} = Disbursement.delete_payroll(payroll)

    conn
    |> put_flash(:info, "Payroll deleted successfully.")
    |> redirect(to: Routes.payroll_path(conn, :index))
  end
end

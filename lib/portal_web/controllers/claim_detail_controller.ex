defmodule PortalWeb.ClaimDetailController do
  use PortalWeb, :controller

  alias Portal.Disbursement
  alias Portal.Disbursement.ClaimDetail

  def index(conn, _params) do
    claim_details = Disbursement.list_claim_details()
    render(conn, "index.html", claim_details: claim_details)
  end

  def new(conn, _params) do
    changeset = Disbursement.change_claim_detail(%ClaimDetail{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"claim_detail" => claim_detail_params}) do
    case Disbursement.create_claim_detail(claim_detail_params) do
      {:ok, claim_detail} ->
        conn
        |> put_flash(:info, "Claim detail created successfully.")
        |> redirect(to: Routes.claim_detail_path(conn, :show, claim_detail))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    claim_detail = Disbursement.get_claim_detail!(id)
    render(conn, "show.html", claim_detail: claim_detail)
  end

  def edit(conn, %{"id" => id}) do
    claim_detail = Disbursement.get_claim_detail!(id)
    changeset = Disbursement.change_claim_detail(claim_detail)
    render(conn, "edit.html", claim_detail: claim_detail, changeset: changeset)
  end

  def update(conn, %{"id" => id, "claim_detail" => claim_detail_params}) do
    claim_detail = Disbursement.get_claim_detail!(id)

    case Disbursement.update_claim_detail(claim_detail, claim_detail_params) do
      {:ok, claim_detail} ->
        conn
        |> put_flash(:info, "Claim detail updated successfully.")
        |> redirect(to: Routes.claim_detail_path(conn, :show, claim_detail))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", claim_detail: claim_detail, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    claim_detail = Disbursement.get_claim_detail!(id)
    {:ok, _claim_detail} = Disbursement.delete_claim_detail(claim_detail)

    conn
    |> put_flash(:info, "Claim detail deleted successfully.")
    |> redirect(to: Routes.claim_detail_path(conn, :index))
  end
end

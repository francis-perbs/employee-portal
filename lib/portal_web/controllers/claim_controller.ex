defmodule PortalWeb.ClaimController do
  use PortalWeb, :controller

  alias Portal.Disbursement
  alias Portal.Disbursement.Claim

  def index(conn, _params) do
    claims = Disbursement.list_claims()
    render(conn, "index.html", claims: claims)
  end

  def new(conn, _params) do
    changeset = Disbursement.change_claim(%Claim{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"claim" => claim_params}) do
    case Disbursement.create_claim(claim_params) do
      {:ok, claim} ->
        conn
        |> put_flash(:info, "Claim created successfully.")
        |> redirect(to: Routes.claim_path(conn, :show, claim))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    claim = Disbursement.get_claim!(id)
    render(conn, "show.html", claim: claim)
  end

  def edit(conn, %{"id" => id}) do
    claim = Disbursement.get_claim!(id)
    changeset = Disbursement.change_claim(claim)
    render(conn, "edit.html", claim: claim, changeset: changeset)
  end

  def update(conn, %{"id" => id, "claim" => claim_params}) do
    claim = Disbursement.get_claim!(id)

    case Disbursement.update_claim(claim, claim_params) do
      {:ok, claim} ->
        conn
        |> put_flash(:info, "Claim updated successfully.")
        |> redirect(to: Routes.claim_path(conn, :show, claim))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", claim: claim, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    claim = Disbursement.get_claim!(id)
    {:ok, _claim} = Disbursement.delete_claim(claim)

    conn
    |> put_flash(:info, "Claim deleted successfully.")
    |> redirect(to: Routes.claim_path(conn, :index))
  end
end

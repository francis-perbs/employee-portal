defmodule PortalWeb.LeaveController do
  use PortalWeb, :controller

  alias Portal.Settings
  alias Portal.Settings.Leave

  def index(conn, _params) do
    leaves = Settings.list_leaves()
    render(conn, "index.html", leaves: leaves)
  end

  def new(conn, _params) do
    changeset = Settings.change_leave(%Leave{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"leave" => leave_params}) do
    case Settings.create_leave(leave_params) do
      {:ok, _leave} ->
        conn
        |> put_flash(:info, "Leave created successfully.")
        |> redirect(to: Routes.leave_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    leave = Settings.get_leave!(id)
    render(conn, "show.html", leave: leave)
  end

  def edit(conn, %{"id" => id}) do
    leave = Settings.get_leave!(id)
    changeset = Settings.change_leave(leave)
    render(conn, "edit.html", leave: leave, changeset: changeset)
  end

  def update(conn, %{"id" => id, "leave" => leave_params}) do
    leave = Settings.get_leave!(id)

    case Settings.update_leave(leave, leave_params) do
      {:ok, leave} ->
        conn
        |> put_flash(:info, "Leave updated successfully.")
        |> redirect(to: Routes.leave_path(conn, :show, leave))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", leave: leave, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    leave = Settings.get_leave!(id)
    {:ok, _leave} = Settings.delete_leave(leave)

    conn
    |> put_flash(:info, "Leave deleted successfully.")
    |> redirect(to: Routes.leave_path(conn, :index))
  end
end

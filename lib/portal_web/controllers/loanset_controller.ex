defmodule PortalWeb.LoansetController do
  use PortalWeb, :controller

  alias Portal.Settings
  alias Portal.Settings.Loanset

  def index(conn, _params) do
    loansets = Settings.list_loansets()
    render(conn, "index.html", loansets: loansets)
  end

  def new(conn, _params) do
    changeset = Settings.change_loanset(%Loanset{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"loanset" => loanset_params}) do
    case Settings.create_loanset(loanset_params) do
      {:ok, _loanset} ->
        conn
        |> put_flash(:info, "Loan created successfully.")
        |> redirect(to: Routes.loanset_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    loanset = Settings.get_loanset!(id)
    render(conn, "show.html", loanset: loanset)
  end

  def edit(conn, %{"id" => id}) do
    loanset = Settings.get_loanset!(id)
    changeset = Settings.change_loanset(loanset)
    render(conn, "edit.html", loanset: loanset, changeset: changeset)
  end

  def update(conn, %{"id" => id, "loanset" => loanset_params}) do
    loanset = Settings.get_loanset!(id)

    case Settings.update_loanset(loanset, loanset_params) do
      {:ok, loanset} ->
        conn
        |> put_flash(:info, "Loan updated successfully.")
        |> redirect(to: Routes.loanset_path(conn, :show, loanset))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", loanset: loanset, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    loanset = Settings.get_loanset!(id)
    {:ok, _loanset} = Settings.delete_loanset(loanset)

    conn
    |> put_flash(:info, "Loan deleted successfully.")
    |> redirect(to: Routes.loanset_path(conn, :index))
  end
end

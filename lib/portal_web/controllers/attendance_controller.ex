defmodule PortalWeb.AttendanceController do
  use PortalWeb, :controller

  alias Portal.Profiles
  alias Portal.Timekeeping
  alias Portal.Timekeeping.Attendance

  def load_selections() do
    employees = Profiles.list_employees()
    %{ employees: employees }
  end

  @spec index(Plug.Conn.t(), any) :: Plug.Conn.t()
  def index(conn, _params) do
    attendances = Timekeeping.list_attendances()
    render(conn, "index.html", attendances: attendances)
  end

  def new(conn, _params) do
    attendance = %Attendance{}
    changeset = Timekeeping.change_attendance(attendance)
    map_selections = load_selections()
    render(conn, "new.html", changeset: changeset, attendance: attendance, map_selections: map_selections)
  end

  def create(conn, %{"attendance" => attendance_params}) do
    case Timekeeping.create_attendance(attendance_params) do
      {:ok, attendance} ->
        conn
        |> put_flash(:info, "Attendance created successfully.")
        |> redirect(to: Routes.attendance_path(conn, :show, attendance))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    attendance = Timekeeping.get_attendance!(id)
    map_selections = load_selections()
    render(conn, "show.html", attendance: attendance, map_selections: map_selections)
  end

  def edit(conn, %{"id" => id}) do
    attendance = Timekeeping.get_attendance!(id)
    #{:ok, localIn} = TzDatetime.original_datetime(attendance, datetime: :dateTimeIn)
    #{:ok, localOut} = TzDatetime.original_datetime(attendance, datetime: :dateTimeOut)
    #%{attendance | input_dateTimeIn: localIn, input_dateTimeOut: localOut}
    changeset = Timekeeping.change_attendance(attendance)
    map_selections = load_selections()
    render(conn, "edit.html", attendance: attendance, changeset: changeset, map_selections: map_selections)
  end

  def update(conn, %{"id" => id, "attendance" => attendance_params}) do
    attendance = Timekeeping.get_attendance!(id)

    case Timekeeping.update_attendance(attendance, attendance_params) do
      {:ok, attendance} ->
        conn
        |> put_flash(:info, "Attendance updated successfully.")
        |> redirect(to: Routes.attendance_path(conn, :show, attendance))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", attendance: attendance, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    attendance = Timekeeping.get_attendance!(id)
    {:ok, _attendance} = Timekeeping.delete_attendance(attendance)

    conn
    |> put_flash(:info, "Attendance deleted successfully.")
    |> redirect(to: Routes.attendance_path(conn, :index))
  end
end

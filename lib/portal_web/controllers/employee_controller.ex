defmodule PortalWeb.EmployeeController do
  use PortalWeb, :controller

  alias Portal.Repo

  alias Portal.Settings
  alias Portal.Profiles
  alias Portal.Profiles.Employee
  alias Portal.Profiles.Empleave
  alias Portal.Profiles.Emploan

  def load_selections() do
    countries = Settings.list_countries()
    positions = Settings.list_positions()
    departments = Settings.list_departments()
    %{ countries: countries, positions: positions, departments: departments }
  end

  def index(conn, _params) do
    employees = Profiles.list_employees()
    render(conn, "index.html", employees: employees)
  end

  def new(conn, _params) do
    changeset = Profiles.change_employee(%Employee{})
    map_selections = load_selections()
    emp_leaves = %Empleave{}
    emp_loans = %Emploan{}
    render(conn, "new.html", changeset: changeset, emp_leaves: emp_leaves, emp_loans: emp_loans, map_selections: map_selections)
  end

  def create(conn, %{"employee" => employee_params}) do
    case Profiles.create_employee(employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee created successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        emp_leaves = %Empleave{}
        emp_loans = %Emploan{}
        render(conn, "new.html", emp_leaves: emp_leaves, emp_loans: emp_loans, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    employee =
      id
      |> Profiles.get_employee!()
      |> Repo.preload([:emp_leaves, :emp_loans])
    map_selections = load_selections()
    render(conn, "show.html", employee: employee, emp_leaves: employee.emp_leaves, emp_loans: employee.emp_loans, map_selections: map_selections)
  end

  def edit(conn, %{"id" => id}) do
    employee =
      id
      |> Profiles.get_employee!()
      |> Repo.preload([:emp_leaves, :emp_loans])
    changeset = Profiles.change_employee(employee)
    map_selections = load_selections()
    render(conn, "edit.html", employee: employee, emp_leaves: employee.emp_leaves, emp_loans: employee.emp_loans, map_selections: map_selections, changeset: changeset)
  end

  def update(conn, %{"id" => id, "employee" => employee_params}) do
    employee = Profiles.get_employee!(id)

    case Profiles.update_employee(employee, employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee updated successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", employee: employee, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    employee = Profiles.get_employee!(id)
    {:ok, _employee} = Profiles.delete_employee(employee)

    conn
    |> put_flash(:info, "Employee deleted successfully.")
    |> redirect(to: Routes.employee_path(conn, :index))
  end

end

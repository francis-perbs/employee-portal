defmodule Portal.Settings do
  @moduledoc """
  The Settings context.
  """

  import Ecto.Query, warn: false
  alias Portal.Repo

  alias Portal.Settings.Country

  @doc """
  Returns the list of countries.

  ## Examples

      iex> list_countries()
      [%Country{}, ...]

  """
  def list_countries do
    Repo.all(Country)
  end

  @doc """
  Gets a single country.

  Raises `Ecto.NoResultsError` if the Country does not exist.

  ## Examples

      iex> get_country!(123)
      %Country{}

      iex> get_country!(456)
      ** (Ecto.NoResultsError)

  """
  def get_country!(id), do: Repo.get!(Country, id)

  @doc """
  Creates a country.

  ## Examples

      iex> create_country(%{field: value})
      {:ok, %Country{}}

      iex> create_country(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_country(attrs \\ %{}) do
    %Country{}
    |> Country.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a country.

  ## Examples

      iex> update_country(country, %{field: new_value})
      {:ok, %Country{}}

      iex> update_country(country, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_country(%Country{} = country, attrs) do
    country
    |> Country.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a country.

  ## Examples

      iex> delete_country(country)
      {:ok, %Country{}}

      iex> delete_country(country)
      {:error, %Ecto.Changeset{}}

  """
  def delete_country(%Country{} = country) do
    Repo.delete(country)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking country changes.

  ## Examples

      iex> change_country(country)
      %Ecto.Changeset{data: %Country{}}

  """
  def change_country(%Country{} = country, attrs \\ %{}) do
    Country.changeset(country, attrs)
  end

  alias Portal.Settings.Department

  @doc """
  Returns the list of departments.

  ## Examples

      iex> list_departments()
      [%Department{}, ...]

  """
  def list_departments do
    Repo.all(Department)
  end

  @doc """
  Gets a single department.

  Raises `Ecto.NoResultsError` if the Department does not exist.

  ## Examples

      iex> get_department!(123)
      %Department{}

      iex> get_department!(456)
      ** (Ecto.NoResultsError)

  """
  def get_department!(id), do: Repo.get!(Department, id)

  @doc """
  Creates a department.

  ## Examples

      iex> create_department(%{field: value})
      {:ok, %Department{}}

      iex> create_department(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_department(attrs \\ %{}) do
    %Department{}
    |> Department.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a department.

  ## Examples

      iex> update_department(department, %{field: new_value})
      {:ok, %Department{}}

      iex> update_department(department, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_department(%Department{} = department, attrs) do
    department
    |> Department.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a department.

  ## Examples

      iex> delete_department(department)
      {:ok, %Department{}}

      iex> delete_department(department)
      {:error, %Ecto.Changeset{}}

  """
  def delete_department(%Department{} = department) do
    Repo.delete(department)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking department changes.

  ## Examples

      iex> change_department(department)
      %Ecto.Changeset{data: %Department{}}

  """
  def change_department(%Department{} = department, attrs \\ %{}) do
    Department.changeset(department, attrs)
  end

  alias Portal.Settings.Position

  @doc """
  Returns the list of positions.

  ## Examples

      iex> list_positions()
      [%Position{}, ...]

  """
  def list_positions do
    Repo.all(Position)
  end

  @doc """
  Gets a single position.

  Raises `Ecto.NoResultsError` if the Position does not exist.

  ## Examples

      iex> get_position!(123)
      %Position{}

      iex> get_position!(456)
      ** (Ecto.NoResultsError)

  """
  def get_position!(id), do: Repo.get!(Position, id)

  @doc """
  Creates a position.

  ## Examples

      iex> create_position(%{field: value})
      {:ok, %Position{}}

      iex> create_position(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_position(attrs \\ %{}) do
    %Position{}
    |> Position.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a position.

  ## Examples

      iex> update_position(position, %{field: new_value})
      {:ok, %Position{}}

      iex> update_position(position, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_position(%Position{} = position, attrs) do
    position
    |> Position.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a position.

  ## Examples

      iex> delete_position(position)
      {:ok, %Position{}}

      iex> delete_position(position)
      {:error, %Ecto.Changeset{}}

  """
  def delete_position(%Position{} = position) do
    Repo.delete(position)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking position changes.

  ## Examples

      iex> change_position(position)
      %Ecto.Changeset{data: %Position{}}

  """
  def change_position(%Position{} = position, attrs \\ %{}) do
    Position.changeset(position, attrs)
  end

  alias Portal.Settings.Leave

  @doc """
  Returns the list of leaves.

  ## Examples

      iex> list_leaves()
      [%Leave{}, ...]

  """
  def list_leaves do
    Repo.all(Leave)
  end

  @doc """
  Gets a single leave.

  Raises `Ecto.NoResultsError` if the Leave does not exist.

  ## Examples

      iex> get_leave!(123)
      %Leave{}

      iex> get_leave!(456)
      ** (Ecto.NoResultsError)

  """
  def get_leave!(id), do: Repo.get!(Leave, id)

  @doc """
  Creates a leave.

  ## Examples

      iex> create_leave(%{field: value})
      {:ok, %Leave{}}

      iex> create_leave(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_leave(attrs \\ %{}) do
    %Leave{}
    |> Leave.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a leave.

  ## Examples

      iex> update_leave(leave, %{field: new_value})
      {:ok, %Leave{}}

      iex> update_leave(leave, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_leave(%Leave{} = leave, attrs) do
    leave
    |> Leave.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a leave.

  ## Examples

      iex> delete_leave(leave)
      {:ok, %Leave{}}

      iex> delete_leave(leave)
      {:error, %Ecto.Changeset{}}

  """
  def delete_leave(%Leave{} = leave) do
    Repo.delete(leave)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking leave changes.

  ## Examples

      iex> change_leave(leave)
      %Ecto.Changeset{data: %Leave{}}

  """
  def change_leave(%Leave{} = leave, attrs \\ %{}) do
    Leave.changeset(leave, attrs)
  end

  alias Portal.Settings.Loanset

  @doc """
  Returns the list of loansets.

  ## Examples

      iex> list_loansets()
      [%Loanset{}, ...]

  """
  def list_loansets do
    Repo.all(Loanset)
  end

  @doc """
  Gets a single loanset.

  Raises `Ecto.NoResultsError` if the Loanset does not exist.

  ## Examples

      iex> get_loanset!(123)
      %Loanset{}

      iex> get_loanset!(456)
      ** (Ecto.NoResultsError)

  """
  def get_loanset!(id), do: Repo.get!(Loanset, id)

  @doc """
  Creates a loanset.

  ## Examples

      iex> create_loanset(%{field: value})
      {:ok, %Loanset{}}

      iex> create_loanset(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_loanset(attrs \\ %{}) do
    %Loanset{}
    |> Loanset.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a loanset.

  ## Examples

      iex> update_loanset(loanset, %{field: new_value})
      {:ok, %Loanset{}}

      iex> update_loanset(loanset, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_loanset(%Loanset{} = loanset, attrs) do
    loanset
    |> Loanset.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a loanset.

  ## Examples

      iex> delete_loanset(loanset)
      {:ok, %Loanset{}}

      iex> delete_loanset(loanset)
      {:error, %Ecto.Changeset{}}

  """
  def delete_loanset(%Loanset{} = loanset) do
    Repo.delete(loanset)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking loanset changes.

  ## Examples

      iex> change_loanset(loanset)
      %Ecto.Changeset{data: %Loanset{}}

  """
  def change_loanset(%Loanset{} = loanset, attrs \\ %{}) do
    Loanset.changeset(loanset, attrs)
  end
end

defmodule Portal.Timekeeping.Attendance do
  use Ecto.Schema
  import Ecto.Changeset

  schema "attendances" do
    #field :input_dateTimeIn, :naive_datetime, virtual: true
    #field :dateTimeIn, :utc_datetime

    #field :input_dateTimeOut, :naive_datetime, virtual: true
    #field :dateTimeOut, :utc_datetime

    #field :time_zone, :string
    #field :original_offset, :integer

    field :employee_id, :integer
    field :dateIn, :date
    field :timeIn, :time
    field :dateOut, :date
    field :timeOut, :time
    field :regHours, :decimal
    field :otHours, :decimal
    field :leaveHours, :decimal
    field :totalHours, :decimal

    timestamps()
  end

  @doc false
  def changeset(attendance, attrs) do
    attendance
    |> cast(attrs, [:employee_id, :dateIn, :timeIn, :dateOut, :timeOut, :regHours, :otHours, :leaveHours, :totalHours])
    |> validate_required([:employee_id, :dateIn, :timeIn, :timeOut, :regHours, :otHours, :leaveHours, :totalHours])
    #|> cast(attrs, [:empCode, :time_zone, :input_dateTimeIn, :input_dateTimeOut, :workType, :workHours])
    #|> validate_required([:empCode, :time_zone, :input_dateTimeIn, :input_dateTimeOut, :workType, :workHours])
    #|> TzDatetime.handle_datetime(input_datetime: :input_dateTimeIn, datetime: :dateTimeIn)
    #|> TzDatetime.handle_datetime(input_datetime: :input_dateTimeOut, datetime: :dateTimeOut)
  end
end

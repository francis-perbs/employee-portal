defmodule Portal.Disbursement do
  @moduledoc """
  The Disbursement context.
  """

  import Ecto.Query, warn: false
  alias Portal.Repo

  alias Portal.Disbursement.Payroll

  @doc """
  Returns the list of payrolls.

  ## Examples

      iex> list_payrolls()
      [%Payroll{}, ...]

  """
  def list_payrolls do
    Repo.all(Payroll)
  end

  @doc """
  Gets a single payroll.

  Raises `Ecto.NoResultsError` if the Payroll does not exist.

  ## Examples

      iex> get_payroll!(123)
      %Payroll{}

      iex> get_payroll!(456)
      ** (Ecto.NoResultsError)

  """
  def get_payroll!(id), do: Repo.get!(Payroll, id)

  @doc """
  Creates a payroll.

  ## Examples

      iex> create_payroll(%{field: value})
      {:ok, %Payroll{}}

      iex> create_payroll(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_payroll(attrs \\ %{}) do
    %Payroll{}
    |> Payroll.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a payroll.

  ## Examples

      iex> update_payroll(payroll, %{field: new_value})
      {:ok, %Payroll{}}

      iex> update_payroll(payroll, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_payroll(%Payroll{} = payroll, attrs) do
    payroll
    |> Payroll.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a payroll.

  ## Examples

      iex> delete_payroll(payroll)
      {:ok, %Payroll{}}

      iex> delete_payroll(payroll)
      {:error, %Ecto.Changeset{}}

  """
  def delete_payroll(%Payroll{} = payroll) do
    Repo.delete(payroll)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking payroll changes.

  ## Examples

      iex> change_payroll(payroll)
      %Ecto.Changeset{data: %Payroll{}}

  """
  def change_payroll(%Payroll{} = payroll, attrs \\ %{}) do
    Payroll.changeset(payroll, attrs)
  end

  alias Portal.Disbursement.Loan

  @doc """
  Returns the list of loans.

  ## Examples

      iex> list_loans()
      [%Loan{}, ...]

  """
  def list_loans do
    Repo.all(Loan)
  end

  @doc """
  Gets a single loan.

  Raises `Ecto.NoResultsError` if the Loan does not exist.

  ## Examples

      iex> get_loan!(123)
      %Loan{}

      iex> get_loan!(456)
      ** (Ecto.NoResultsError)

  """
  def get_loan!(id), do: Repo.get!(Loan, id)

  @doc """
  Creates a loan.

  ## Examples

      iex> create_loan(%{field: value})
      {:ok, %Loan{}}

      iex> create_loan(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_loan(attrs \\ %{}) do
    %Loan{}
    |> Loan.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a loan.

  ## Examples

      iex> update_loan(loan, %{field: new_value})
      {:ok, %Loan{}}

      iex> update_loan(loan, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_loan(%Loan{} = loan, attrs) do
    loan
    |> Loan.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a loan.

  ## Examples

      iex> delete_loan(loan)
      {:ok, %Loan{}}

      iex> delete_loan(loan)
      {:error, %Ecto.Changeset{}}

  """
  def delete_loan(%Loan{} = loan) do
    Repo.delete(loan)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking loan changes.

  ## Examples

      iex> change_loan(loan)
      %Ecto.Changeset{data: %Loan{}}

  """
  def change_loan(%Loan{} = loan, attrs \\ %{}) do
    Loan.changeset(loan, attrs)
  end

  alias Portal.Disbursement.Claim

  @doc """
  Returns the list of claims.

  ## Examples

      iex> list_claims()
      [%Claim{}, ...]

  """
  def list_claims do
    Repo.all(Claim)
  end

  @doc """
  Gets a single claim.

  Raises `Ecto.NoResultsError` if the Claim does not exist.

  ## Examples

      iex> get_claim!(123)
      %Claim{}

      iex> get_claim!(456)
      ** (Ecto.NoResultsError)

  """
  def get_claim!(id), do: Repo.get!(Claim, id)

  @doc """
  Creates a claim.

  ## Examples

      iex> create_claim(%{field: value})
      {:ok, %Claim{}}

      iex> create_claim(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_claim(attrs \\ %{}) do
    %Claim{}
    |> Claim.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a claim.

  ## Examples

      iex> update_claim(claim, %{field: new_value})
      {:ok, %Claim{}}

      iex> update_claim(claim, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_claim(%Claim{} = claim, attrs) do
    claim
    |> Claim.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a claim.

  ## Examples

      iex> delete_claim(claim)
      {:ok, %Claim{}}

      iex> delete_claim(claim)
      {:error, %Ecto.Changeset{}}

  """
  def delete_claim(%Claim{} = claim) do
    Repo.delete(claim)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking claim changes.

  ## Examples

      iex> change_claim(claim)
      %Ecto.Changeset{data: %Claim{}}

  """
  def change_claim(%Claim{} = claim, attrs \\ %{}) do
    Claim.changeset(claim, attrs)
  end

  alias Portal.Disbursement.ClaimDetail

  @doc """
  Returns the list of claim_details.

  ## Examples

      iex> list_claim_details()
      [%ClaimDetail{}, ...]

  """
  def list_claim_details do
    Repo.all(ClaimDetail)
  end

  @doc """
  Gets a single claim_detail.

  Raises `Ecto.NoResultsError` if the Claim detail does not exist.

  ## Examples

      iex> get_claim_detail!(123)
      %ClaimDetail{}

      iex> get_claim_detail!(456)
      ** (Ecto.NoResultsError)

  """
  def get_claim_detail!(id), do: Repo.get!(ClaimDetail, id)

  @doc """
  Creates a claim_detail.

  ## Examples

      iex> create_claim_detail(%{field: value})
      {:ok, %ClaimDetail{}}

      iex> create_claim_detail(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_claim_detail(attrs \\ %{}) do
    %ClaimDetail{}
    |> ClaimDetail.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a claim_detail.

  ## Examples

      iex> update_claim_detail(claim_detail, %{field: new_value})
      {:ok, %ClaimDetail{}}

      iex> update_claim_detail(claim_detail, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_claim_detail(%ClaimDetail{} = claim_detail, attrs) do
    claim_detail
    |> ClaimDetail.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a claim_detail.

  ## Examples

      iex> delete_claim_detail(claim_detail)
      {:ok, %ClaimDetail{}}

      iex> delete_claim_detail(claim_detail)
      {:error, %Ecto.Changeset{}}

  """
  def delete_claim_detail(%ClaimDetail{} = claim_detail) do
    Repo.delete(claim_detail)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking claim_detail changes.

  ## Examples

      iex> change_claim_detail(claim_detail)
      %Ecto.Changeset{data: %ClaimDetail{}}

  """
  def change_claim_detail(%ClaimDetail{} = claim_detail, attrs \\ %{}) do
    ClaimDetail.changeset(claim_detail, attrs)
  end
end

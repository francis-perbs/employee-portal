defmodule Portal.Profiles do
  @moduledoc """
  The Profiles context.
  """

  import Ecto.Query, warn: false
  alias Portal.Repo

  alias Portal.Profiles.Employee

  @doc """
  Returns the list of employees.

  ## Examples

      iex> list_employees()
      [%Employee{}, ...]

  """
  def list_employees do
    Repo.all(Employee)
  end

  @doc """
  Gets a single employee.

  Raises `Ecto.NoResultsError` if the Employee does not exist.

  ## Examples

      iex> get_employee!(123)
      %Employee{}

      iex> get_employee!(456)
      ** (Ecto.NoResultsError)

  """
  def get_employee!(id), do: Repo.get!(Employee, id)

  @doc """
  Creates a employee.

  ## Examples

      iex> create_employee(%{field: value})
      {:ok, %Employee{}}

      iex> create_employee(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_employee(attrs \\ %{}) do
    %Employee{}
    |> Employee.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a employee.

  ## Examples

      iex> update_employee(employee, %{field: new_value})
      {:ok, %Employee{}}

      iex> update_employee(employee, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_employee(%Employee{} = employee, attrs) do
    employee
    |> Employee.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a employee.

  ## Examples

      iex> delete_employee(employee)
      {:ok, %Employee{}}

      iex> delete_employee(employee)
      {:error, %Ecto.Changeset{}}

  """
  def delete_employee(%Employee{} = employee) do
    Repo.delete(employee)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking employee changes.

  ## Examples

      iex> change_employee(employee)
      %Ecto.Changeset{data: %Employee{}}

  """
  def change_employee(%Employee{} = employee, attrs \\ %{}) do
    Employee.changeset(employee, attrs)
  end

  # def add_leave(employee_id, emp_leave_params) do
  #   emp_leave_params
  #   |> Map.put("employee_id", employee_id)d
  #   |> create_emp_leave()
  # end

  alias Portal.Profiles.Empleave

  @doc """
  Returns the list of emp_leaves.

  ## Examples

      iex> list_emp_leaves()
      [%Empleave{}, ...]

  """
  def list_emp_leaves do
    Repo.all(Empleave)
  end

  @doc """
  Gets a single empleave.

  Raises `Ecto.NoResultsError` if the Empleave does not exist.

  ## Examples

      iex> get_empleave!(123)
      %Empleave{}

      iex> get_empleave!(456)
      ** (Ecto.NoResultsError)

  """
  def get_empleave!(id), do: Repo.get!(Empleave, id)

  @doc """
  Creates a empleave.

  ## Examples

      iex> create_empleave(%{field: value})
      {:ok, %Empleave{}}

      iex> create_empleave(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_empleave(attrs \\ %{}) do
    %Empleave{}
    |> Empleave.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a empleave.

  ## Examples

      iex> update_empleave(empleave, %{field: new_value})
      {:ok, %Empleave{}}

      iex> update_empleave(empleave, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_empleave(%Empleave{} = empleave, attrs) do
    empleave
    |> Empleave.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a empleave.

  ## Examples

      iex> delete_empleave(empleave)
      {:ok, %Empleave{}}

      iex> delete_empleave(empleave)
      {:error, %Ecto.Changeset{}}

  """
  def delete_empleave(%Empleave{} = empleave) do
    Repo.delete(empleave)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking empleave changes.

  ## Examples

      iex> change_empleave(empleave)
      %Ecto.Changeset{data: %Empleave{}}

  """
  def change_empleave(%Empleave{} = empleave, attrs \\ %{}) do
    Empleave.changeset(empleave, attrs)
  end

  alias Portal.Profiles.Emploan

  @doc """
  Returns the list of emp_loans.

  ## Examples

      iex> list_emp_loans()
      [%Emploan{}, ...]

  """
  def list_emp_loans do
    Repo.all(Emploan)
  end

  @doc """
  Gets a single emploan.

  Raises `Ecto.NoResultsError` if the Emploan does not exist.

  ## Examples

      iex> get_emploan!(123)
      %Emploan{}

      iex> get_emploan!(456)
      ** (Ecto.NoResultsError)

  """
  def get_emploan!(id), do: Repo.get!(Emploan, id)

  @doc """
  Creates a emploan.

  ## Examples

      iex> create_emploan(%{field: value})
      {:ok, %Emploan{}}

      iex> create_emploan(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_emploan(attrs \\ %{}) do
    %Emploan{}
    |> Emploan.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a emploan.

  ## Examples

      iex> update_emploan(emploan, %{field: new_value})
      {:ok, %Emploan{}}

      iex> update_emploan(emploan, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_emploan(%Emploan{} = emploan, attrs) do
    emploan
    |> Emploan.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a emploan.

  ## Examples

      iex> delete_emploan(emploan)
      {:ok, %Emploan{}}

      iex> delete_emploan(emploan)
      {:error, %Ecto.Changeset{}}

  """
  def delete_emploan(%Emploan{} = emploan) do
    Repo.delete(emploan)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking emploan changes.

  ## Examples

      iex> change_emploan(emploan)
      %Ecto.Changeset{data: %Emploan{}}

  """
  def change_emploan(%Emploan{} = emploan, attrs \\ %{}) do
    Emploan.changeset(emploan, attrs)
  end
end

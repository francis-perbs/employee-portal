defmodule Portal.Profiles.Employee do
  use Ecto.Schema
  import Ecto.Changeset

  alias Portal.Profiles.Empleave
  alias Portal.Profiles.Emploan

  schema "employees" do
    field :allowance, :decimal
    field :bankAcctNbr, :string
    field :basicSalary, :decimal
    field :birthDate, :date
    field :contactNbr, :string
    field :countryID, :integer
    field :departmentID, :integer
    field :email, :string
    field :employeeCode, :string
    field :employeeStatus, :string
    field :firstName, :string
    field :gender, :string
    field :hdmfNbr, :string
    field :hdmfPrem, :decimal
    field :hireDate, :date
    field :hmoNbr, :string
    field :jobTitleID, :integer
    field :lastName, :string
    field :middleName, :string
    field :permanentAddr, :string
    field :philhealthNbr, :string
    field :philhealthPrem, :decimal
    field :presentAddr, :string
    field :sssNbr, :string
    field :sssPrem, :decimal
    field :status, :string
    field :taxStatus, :string
    field :taxWth, :decimal
    field :terminationDate, :date
    field :tinNbr, :string
    has_many :emp_leaves, Empleave
    has_many :emp_loans, Emploan

    timestamps()
  end

  @doc false
  def changeset(employee, attrs) do
    employee
    |> cast(attrs, [:employeeCode, :firstName, :middleName, :lastName, :jobTitleID, :departmentID, :gender, :status, :birthDate, :presentAddr, :permanentAddr, :countryID, :contactNbr, :email, :employeeStatus, :hireDate, :terminationDate, :sssNbr, :tinNbr, :philhealthNbr, :hdmfNbr, :hmoNbr, :bankAcctNbr, :basicSalary, :taxStatus, :sssPrem, :philhealthPrem, :hdmfPrem, :taxWth, :allowance])
    |> validate_required([:employeeCode, :firstName, :middleName, :lastName, :jobTitleID, :departmentID, :gender, :status, :birthDate, :presentAddr, :permanentAddr, :countryID, :contactNbr, :email, :employeeStatus, :hireDate, :sssNbr, :tinNbr, :philhealthNbr, :hdmfNbr, :hmoNbr, :bankAcctNbr, :basicSalary, :taxStatus, :sssPrem, :philhealthPrem, :hdmfPrem, :taxWth, :allowance])
    |> unique_constraint(:employeeCode)
  end
end

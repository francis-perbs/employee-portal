defmodule Portal.Profiles.Emploan do
  use Ecto.Schema
  import Ecto.Changeset

  schema "emp_loans" do
    field :loanDate, :date
    field :loanName, :string
    field :payableMonths, :decimal
    field :principalAmount, :decimal
    field :employee_id, :id

    timestamps()
  end

  @doc false
  def changeset(emploan, attrs) do
    emploan
    |> cast(attrs, [:loanName, :loanDate, :principalAmount, :payableMonths, :employee_id])
    |> validate_required([:loanName, :loanDate, :principalAmount, :payableMonths, :employee_id])
    |> foreign_key_constraint(:employee_id)
  end
end

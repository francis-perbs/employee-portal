defmodule Portal.Profiles.Empleave do
  use Ecto.Schema
  import Ecto.Changeset

  schema "emp_leaves" do
    field :leaveQty, :decimal
    field :leaveType, :string
    field :employee_id, :id
    # belongs_to :employee, Portal.Profiles.Employee

    timestamps()
  end

  @doc false
  def changeset(empleave, attrs) do
    empleave
    |> cast(attrs, [:leaveType, :leaveQty, :employee_id])
    |> validate_required([:leaveType, :leaveQty, :employee_id])
    |> foreign_key_constraint(:employee_id)
  end
end

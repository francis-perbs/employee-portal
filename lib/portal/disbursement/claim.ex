defmodule Portal.Disbursement.Claim do
  use Ecto.Schema
  import Ecto.Changeset

  schema "claims" do
    field :description, :string
    field :docDate, :date
    field :empCode, :string
    field :expenseClaimDate, :date
    field :expenseClaimNbr, :string
    field :status, :string
    field :totalAmt, :decimal

    timestamps()
  end

  @doc false
  def changeset(claim, attrs) do
    claim
    |> cast(attrs, [:expenseClaimNbr, :description, :status, :expenseClaimDate, :totalAmt, :docDate, :empCode])
    |> validate_required([:expenseClaimNbr, :description, :status, :expenseClaimDate, :totalAmt, :docDate, :empCode])
  end
end

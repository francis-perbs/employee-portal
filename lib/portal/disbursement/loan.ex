defmodule Portal.Disbursement.Loan do
  use Ecto.Schema
  import Ecto.Changeset

  schema "loans" do
    field :employee_id, :integer
    field :loanID, :integer
    field :principal, :decimal
    field :monthlyPaymentDate, :date
    field :amount, :decimal
    field :interest, :decimal
    field :amountDue, :decimal
    field :amountPaid, :decimal

    timestamps()
  end

  @doc false
  def changeset(loan, attrs) do
    loan
    |> cast(attrs, [:employee_id, :loanID, :principal, :monthlyPaymentDate, :amount, :interest, :amountDue, :amountPaid])
    |> validate_required([:employee_id, :loanID, :principal, :monthlyPaymentDate, :amount, :interest])
  end
end

defmodule Portal.Disbursement.ClaimDetail do
  use Ecto.Schema
  import Ecto.Changeset

  schema "claim_details" do
    field :description, :string
    field :expenseClaimNbr, :string
    field :expenseType, :string
    field :lineAmt, :decimal

    timestamps()
  end

  @doc false
  def changeset(claim_detail, attrs) do
    claim_detail
    |> cast(attrs, [:expenseClaimNbr, :expenseType, :description, :lineAmt])
    |> validate_required([:expenseClaimNbr, :expenseType, :description, :lineAmt])
  end
end

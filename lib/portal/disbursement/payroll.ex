defmodule Portal.Disbursement.Payroll do
  use Ecto.Schema
  import Ecto.Changeset

  schema "payrolls" do
    field :absences, :decimal
    field :allowances, :decimal
    field :beginningDate, :date
    field :bonus, :decimal
    field :cashAdvances, :decimal
    field :commission, :decimal
    field :cutoffDate, :date
    field :empCode, :string
    field :loan, :decimal
    field :payrollCode, :string
    field :status, :string
    field :tardiness, :decimal
    field :thirteenthMonth, :decimal

    timestamps()
  end

  @doc false
  def changeset(payroll, attrs) do
    payroll
    |> cast(attrs, [:empCode, :payrollCode, :beginningDate, :cutoffDate, :status, :bonus, :thirteenthMonth, :allowances, :commission, :tardiness, :absences, :loan, :cashAdvances])
    |> validate_required([:empCode, :payrollCode, :beginningDate, :cutoffDate, :status, :bonus, :thirteenthMonth, :allowances, :commission, :tardiness, :absences, :loan, :cashAdvances])
  end
end

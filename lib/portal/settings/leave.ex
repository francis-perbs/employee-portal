defmodule Portal.Settings.Leave do
  use Ecto.Schema
  import Ecto.Changeset

  schema "leaves" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(leave, attrs) do
    leave
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end

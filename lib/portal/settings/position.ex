defmodule Portal.Settings.Position do
  use Ecto.Schema
  import Ecto.Changeset

  schema "positions" do
    field :position, :string

    timestamps()
  end

  @doc false
  def changeset(position, attrs) do
    position
    |> cast(attrs, [:position])
    |> validate_required([:position])
  end
end

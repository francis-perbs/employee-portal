defmodule Portal.Settings.Department do
  use Ecto.Schema
  import Ecto.Changeset

  schema "departments" do
    field :departmentName, :string

    timestamps()
  end

  @doc false
  def changeset(department, attrs) do
    department
    |> cast(attrs, [:departmentName])
    |> validate_required([:departmentName])
  end
end

defmodule Portal.Settings.Country do
  use Ecto.Schema
  import Ecto.Changeset

  schema "countries" do
    field :countryCD, :string
    field :countryName, :string

    timestamps()
  end

  @doc false
  def changeset(country, attrs) do
    country
    |> cast(attrs, [:countryCD, :countryName])
    |> validate_required([:countryCD, :countryName])
    |> unique_constraint(:countryCD, name: :countries_country_code_unique_index)
  end
end

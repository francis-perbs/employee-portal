defmodule Portal.Settings.Loanset do
  use Ecto.Schema
  import Ecto.Changeset

  schema "loansets" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(loanset, attrs) do
    loanset
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
